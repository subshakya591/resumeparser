from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import  shutil
import pickle
import json
from fileReader import FileReader
from segmentExtractor import ResumeSegmentCreator
from informationParser import InformationParser
from flask import Flask, request, render_template, send_from_directory, jsonify

app = Flask(__name__)

APP_ROOT = os.path.dirname(os.path.abspath(__file__))

with open(os.getcwd()+'/models/segment_identifier.pkl','rb')as pkl:
    model=pickle.load(pkl)
def move_file():
    source_path=os.getcwd()+'/resume_input/'
    destination_path=os.getcwd()+'/collected CV/'
    files=os.listdir(source_path)
    for file in files:
        src=source_path+file
        dstn=destination_path+file
        shutil.move(src,dstn)
    print("<+++++++@FilesMovedSucessfully+++++++>")
    return "sucessfully moved"

def format_education(education):
    degree=education[0]
    university=education[1]
    entry_date=education[2]
    exit_date=education[3]
    return degree,university,entry_date,exit_date
#Upload new Faces
@app.route("/parse", methods=["POST","GET"])
def resumeParser():
    print("<+++++++@API SUCESSFULLY CALLED+++++++>")
    file=request.files.get("resume_input")
    print(file)
    filename = file.filename
    file.save(os.getcwd()+'/resume_input/'+str(filename))
    cv_content = FileReader.read_cv()

    resume_segment = ResumeSegmentCreator(model, cv_content)
    Personal_informations, Objectives, Experiences, Skills, \
    Projects, Educations, Rewards, Languages, References = \
        resume_segment.format_segment()
    print("Personal info", Personal_informations)
    print("objectives", Objectives)
    print("Educations", Educations)
    print("Rewards", Rewards)
    print("Languages", Languages)
    print("Projects", Projects)
    print("Refer", References)
    print("Experiences", Experiences)
    print("skills", Skills)

    # resume_parser=InformationParser(cv_content,
    #                                 sliced_text["profile information"],
    #                                 sliced_text["Objective"],
    #                                 sliced_text["Experience:"],
    #                                 sliced_text["Technical Skills"],
    #                                 default_list,#projects
    #                                 sliced_text["Education"],
    #                                 default_list,#rewards
    #                                 default_list,#languages
    #                                 default_list,)#referemces
    resume_parser = InformationParser(cv_content,
                                      Personal_informations,
                                      Objectives,
                                      Experiences,
                                      Skills,
                                      Projects,  # projects
                                      Educations,
                                      Rewards,  # rewards
                                      Languages,  # languages
                                      References, )  # referemces

    print(resume_parser.personal_information_parser())
    first_name, last_name, email, phone_number,address = resume_parser.personal_information_parser()
    print(first_name)
    objective = resume_parser.objective_parser()
    skill = resume_parser.skills_parser()
    degree, university, entry_date, exit_date = format_education(resume_parser.education_parser())
    rewards = resume_parser.rewards_parser()
    ex_company, designation, entry_date, exit_date = resume_parser.experience_parser()
    language = resume_parser.language_parser()
    references = resume_parser.references_parser()
    projects = resume_parser.project_parser()

    # format_education(resume_parser.education_parser())

    output=({
            'PERSONAL INFORMATION':
                {
                    'First Name': '{}'.format(first_name),
                    'Last Name': '{}'.format(last_name),
                    'Email': email,
                    'Phone Number': phone_number,
                    'Address':address
                },
            'OBJECTIVE':
                {
                    'Objective': objective
                },
            'SKILLS':
                {
                    'Skills': skill
                },
            'EDUCATION':
                {
                    'Degree': degree,
                    'University': university,
                    'Enrolled Year': entry_date,
                    'Graguated Year': exit_date

                },
            'EXPERIENCE':
                {
                    'Company': ex_company,
                    'Designation': designation

                },
            'PROJECTS':
                {
                    'projects': projects
                },

            'REWARDS':
                {
                    'Rewards': rewards
                },
            'LANGUAGES':
                {
                    'Language': language
                },
            'REFERENCES':
                {
                    'References': references
                }

        }
    )

    move_file()
    return jsonify(output)



if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5665, debug=True)






    #
    # print("extracted_experience",resume_parser.experience_parser())
    # print(Experiences)

    # print("extracted_languages",resume_parser.language_parser())


