ava Developer
Irving, TX

Work Experience
Java Developer

� Overall 6 years of experience in developing various web, Standalone Software Applications 
� Expertise in full software Development Life Cycle(Design, Development, Testing, 
Deployment and maintenance) 
� Experience in developing web based applications using Java, J2EE, JSP, Servlets, 
Spring, JDBC, JMS, HTML, CSS, XML, Oracle. 
� Expertise in CORBA framework, IBM WebSphere MQ, JBoss Fuse and Java. 
� Good experience in developing UNIX shell scripts. 
� Experience in Enterprise Integration Development using Apache Camel Framework 
� Strong experience with Oracle 11g, SQL Server 2000 and RDBMS concepts. 
� Competent in using Java IDE tools of Eclipse, Red Hat JBoss Dev Studio 
� Expertise on entire SDLC using Web/Application Servers like Tomcat, Fuse servers. 
� Experienced in Software Development Processes like Waterfall and Agile Methodologies. 
� Extensively worked in Unified Modeling Tools (UML) in designing Use Cases, Class 
Diagrams, Sequence and Object Diagrams using Rational Rose. 
� Developed Maven & ANT scripts in to build, deploy J2EE Applications & integrations. 
� Experienced in deploying J2EE Applications archives (JAR, WAR) on server. 
� Good in implementing the SOAP and RESTful web services. 
� Experience in unit, integration testing. 
� Worked on Orchestration with Message Routing, Direct Binding, Exception, WCF-SQL 
Adapter 
� Familiar with SOA and MicroServices Architecture 
� Used HTTP, FTP, MQ-series, SQL, SOAP adapters in BizTalk 2010 for various transactions 
� Experience using Fuse/Apache ServiceMix as integration platform and Apache ActiveMQ as 
Messaging platform. 
� Knowledge of using Apache Camel through Fuse Mediation Router through blueprint & 
Spring. 
� Excellent communication skills to deal with people at all levels. 
� Self-motivated team player with good Analytical, Logical and Problem Solving ability. 
� Implemented few ideas in project which helped in saving efforts of client.

Education
Bachelor of Technology in Technology

Jawaharlal Nehru Technological University
Skills
.NET (Less than 1 year), ANT (Less than 1 year), C+ (Less than 1 year), ClearCase (Less than 1 year), Databases (Less than 1 year)
Additional Information
Technical Skills: 
 
Tools: Eclipse, Altova XMLSpy, Rational ClearCase, ANT, Maven, Git, SOAP UI, JBoss developer Studio, MQ Explorer, SVN, IIS, Microsoft Visual Studio, TFS 
Servers: Oracle 11g [�] IBM WebSphere MQ, Tomcat 6.0/7.0, JBoss Fuse Server 
Languages: Java, C++, UNIX shell scripting, PL/SQL, perl 
Databases: Oracle, SQL Server, Teradata 
 
Operating Systems: Windows XP/7, Linux, UNIX 
Java/Web technology: J2EE, Java Script, JDBC, HTML, XML, JSP, Servlet