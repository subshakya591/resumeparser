Java Devloper
J2EE Developer - State Of Ohio
Coppell, TX

� Over 7+ years of professional hands on experience in software development life cycle projects in Analysis, Designing, Coding, Testing and Implementation and Enhancement of Complex Enterprise Applications. 
� Extensive experience in JAVA/J2EE technologies like Core java, Servlets, JSP, JSTL, JDBC, Hibernate, Struts 1.2, Spring, MVC architecture. 
� Expert Level experience in development using Web markup languages like HTML, XHTML, CSS, XML. 
� Extensively worked on Java-J2EE technologies like JDBC, Servlets, JSP, JSTL, JavaScript, AJAX 
� Extensive experience in Scripting Technologies such as JavaScript, AJAX AND JSON. 
� Experience in XML technologies like XML, Xml Schema, XSLT and parsing methodologies like DOM, SAX. 
� Experience in RDBMS such as Oracle, DB2, SQL Server, MySQL and writing SQL queries. 
� Exposure to developing J2EE applications using IDEs like RAD, Web logic, Eclipse, Eclipse Ganymede. 
� Exposure to installing web based applications in application servers such as Websphere, Web Logic, JBOSS, and Tomcat. 
� Excellent team player with good communication and written skills along with a quest and zeal to learn new technologies. 
� Exposure to Full Life Cycle Solution Development (Requirements, Analysis, Design, Implementation, Deployment, Documentation, Testing, User Training & Support). 
� Individual with an eye for detail and ability to work as part of a team or independently. 
� Excellent Analytical, Problem-solving and Documentation skills to evaluate current process, maturity of the organization and make recommendations for the improvement. 
� Worked with Business & Development team members to understand business scenarios and system functionality in order to improve the testing quality.

Authorized to work in the US for any employer

Work Experience
Java Devloper

Education
Quality Center
Skills
J2EE (7 years), JAVA (7 years), JSP (7 years), JAVASCRIPT (7 years), SERVLETS (4 years), SQL (4 years), AJAX (4 years)
Additional Information
TECHNICAL SKILLS: 
 
Mark-up/ Scripting Lang. HTML, CSS, XML, XSLT, JavaScript, AJAX. 
Programming Languages, Frameworks, J2EE Core Java, Servlets, JSP, Struts, Spring, Hibernate, JSP, JNDI, and UML. 
Databases / RDBMS Oracle10.x, DB2, MY SQL, JDBC. 
Application Server Web Logic, Tomcat, JBoss5.1, WebSphere