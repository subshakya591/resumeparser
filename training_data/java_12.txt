Java /J2EE Developer
Java /J2EE Developer - UPS
Dallas, TX, TX

? 6 years of experience in Application Development, including design and development of projects in the areas of Java/J2EEE client and server side Enterprise applications 
? Extensive experience in designing professional UI web applications using front-end technologies like HTML/HTML5, CSS/CSS3, JavaScript, XML, DHTML, XHTML, JQuery, AJAX, JSON, AngularJS, Angular 2.0, Backbone JS, Require JS, React JS and Bootstrap. 
? Good experience developing enterprise applications using Spring, Hibernate, JavaBeans, Servlets, Struts, XML Mapping, Ant and Maven. 
? Strong experience in design/development of web applications using Java, JSP, J2EE, Servlets, JMS, MVC Framework, Web Services, XML, XSD, JSON, and JAXB. 
? Experience in implementing Core Java concepts and strong understanding of Garbage Collector, SWING, Collections, Multithreading, Event handling', Exception handling, Generics and Serialization. 
? Experience in implementing Java8 features and like parallel streams, lambdas and filters. 
? Expertise in developing and consuming REST and SOAP based Web Services. 
? Excellent Java development skills with core Java8, JDBC, Hibernate and solid experience in MVC architecture, using Spring 3, Spring MVC, Struts 2. 
? Expertise in IDEs for software development like NetBeans, Eclipse, Eclipse STS, RAD, and JDeveloper. 
? Experience in working with various Application Servers like IBM Web Sphere, Web Logic, JBoss and Apache Tomcat Servers. 
? Experience in Developing and Deploying Enterprise Java Beans on Application Servers like WebLogic, WebSphere and JBoss. 
? Profound knowledge on various XML technologies i.e., writing DTD, Schema, Namespaces, XSL, Xpath, Xlink, XQuery, XHTML and SAX, DOM, JDOM parsers. 
? Proficient in Developing MVC framework using Groovy on Grails, Spring 3.0 (IOC, MVC, AOP), ORM framework using Hibernate, GORM for online & batch enterprise applications. 
? Experience in Database Design, Creation and management of Schemas, writing Stored Procedures, functions, Triggers, DDL, DML SQL queries. 
? Experience in writing SQL and PL/SQL programming. 
? Expert in Various Agile methodologies like SCRUM, Test Driven Development, Incremental & Iteration methodology and Pair Programming as well as implementing Waterfall model. To track the progress of Agile process used JIRA. 
? Used Hibernate ORM tools which automate the mapping between SQL databases and objects in Java. Integrated the Spring and Hibernate framework. 
? Experienced in Unit Testing and generating of Test cases for web applications. 
? Expertise in working with cloud services like AWS and NoSQL databases like Cassandra, Mongo. 
? Experience with unit testing the applications using Junit and JMeter. 
? A resourceful team player with good inter-personal skills, effective problem - solving and decision-making skills, looking for a satisfying career in a fair working environment with opportunities for growth. 
? Experience in Bootstrap (responsive design), Backbone, and AngularJS frameworks 
? Experience programming in the JBOSS Enterprise SOA environment including JBOSS Workflow and Drools Business Rules engine. 
? Experienced Implementing MicroServices. We had worked in an Enterprise Application where we had to implement MicroServices to separate the tasks and not to have dependency on other Parallel on-going tasks of same Application. 
? Experienced in MEAN stack advancement (MongoDB, Express.JS, AngularJS &NodeJS) 
? Involved in building and deploying Apache ANT/ MAVEN scripts for deployment and customizing WAR/ JAR file management for web applications. 
? Well experienced in using JUnit for performing unit-testing framework.

Work Experience
Java /J2EE Developer

UPS - Dallas, TX
June 2017 to Present

Responsibilities: 
? Involved in analysis, design, and developing middleware using Servlets, Java Beans, Hibernate, springs and doing enhancements and issues resolutions. 
? Extensive uses of AngularJS directives to get JSON data using AJAX from RESTful API and modify DOM. 
? Designed the presentation layer GUI using JavaScript, JSP, HTML, Groovy, CSS, Angular.JS, Customs tags and developed Client-Side validations. 
? Experience in developing applications using Bootstrap like Grids, Toolbars, Panels, Combo-Box and Button etc. 
? Developed Single page application and structured the JavaScript code using AngularJS. 
? Followed Single Page Application (SPA) Pattern and used AngularJS MVC to build a cross-browser complaint application. 
? Used Apache Kafka (Message Queues) for reliable and asynchronous exchange of important information between multiple business applications. 
? Built applications for scale using Typescript, AngularJS. 
? Implemented continuous deployment system with Jenkins, AWS: Code Deploy, Ops Works, and Packer for deployment of Java Applications. 
? Converted a monolithic app to MicroServices architecture using Spring Boot using 12 factor app methodologies. Deployed, Scaled, Configured, wrote manifest file for various Microservices in PCF. 
? Implemented REST Microservices using Spring boot. Generated Metrics with method level granularity and Persistence using Spring AOP and Spring Actuator. 
? Design roles and groups for users and resources using AWS Identity Access Management (IAM) 
? Developed the DAO layer for the application using spring, Hibernate and developed various logics and reports using HQL and Hibernate criteria. 
? Used Hibernate as ORM to map Java classes to data base tables. 
? Implemented Spring MVC architecture and increased modularity by allowing the separation of crosscutting concerns using Spring Boot. 
? Involved with Spring IOC concepts to integrate Hibernate DAO. 
? Involved in writing Spring Configuration XML, file that contains declarations and business classes are wired-up to the frontend managed beans using Spring IOC pattern. 
? Developed Web Services for data transfer using Restful Webservices. 
? Extensively used Core Spring Framework for Dependency Injections of components and Spring JDBC for managing all the database communication and exception handling. 
? Implemented Multithread /Executor Thread to concurrent for distributed SOAP Web Services. 
? Extensively worked with collections classes like Array List, Hash Map, Iterator, concurrent etc. 
 
Environment: Java 8, JavaScripts, HTML, AngularJS, Angular 2.0/4.0, STRUTS, JUNIT, Servlets, JDBC, JQUERY, JSON, AJAX, DOM, Jenkins, HQL, Spring Boot, Bootstrap, Groovy, Microservices, Selenium, JIRA, Hibernate, DB2, JPA, Eclipse, EJB, JMS, Knockout.js, log4j, NO/SQL, Postgresql.

Java /J2EE Developer

VSP Vision Care - Rancho Cordova, CA
September 2016 to May 2017

Responsibilities: 
? Developed the web layer using Spring MVC framework. 
? Implemented JDBC for mapping an object-oriented domain model to a traditional relational database. 
? Created Stored Procedures to manipulate the database and to apply the business logic according to the user's specifications. 
? Front end development with jQuery, Knockout.js, React, Node.js. 
? Involved in analyzing, designing, implementing and testing of the project. 
? Developed UML diagrams like Use cases and Sequence diagrams as per requirement. 
? Developed the Generic Classes, which includes the frequently used functionality, for reusability. 
? Experience on scripting (e.g. Bash, Powershell, Perl, Ruby, Python) 
? Expertise in web front-end technologies such as, Javascript, Ajax, JSF, Groovy, AngularJS, Bootstrap, CSS, HTML and/or Flex and developing browser compatible applications 
? Involved in Database design and developing SQL Queries, stored procedures on MySQL. 
? Designed and implemented the application's configuration system, including Swing based GUI. 
? Involved in creation of GUI using Java Swing. 
? Functional and Non-functional testing of web based applications, Database and Hbase based Processing tools, Solr Search Engine, and Cloud based web based applications. 
? Peer code reviews using JIRA 
? Developed code to fetch data from back-end and populate on UI using AJAX, and JSON. 
? Worked on fixing bugs and used JIRA to update them. 
? Developed user interface using JSP, HTML, XHTML, Microservices, XSLT and JavaScript to simplify the complexities of the application. 
? Highly skilled in using Version Control Management tools like SVN, CVS, and GIT 
 
Environment: JAVA, JavaScript, HTML, AngularJS, Bootstrap, Groovy, Microservices, JDBC Drivers, SOAP Web Services, UNIX, Shell scripting, SQL Server, CSS, MVC, JSF, GIT, AJAX.

Java Developer

Experian - Costa Mesa, CA
June 2015 to July 2016

Responsibilities: 
? Developed the single page application following Agile Scrum, participated in daily standups, sprint reviews, Kickoffs and used Jira for project management. 
? Involved in the presentation layer development using HTML, JQuery, Groovy, Bootstrap, JQuery UI, JavaScript, JSON and AJAX. 
? Built AngularJS framework including MVC architectures, different modules, specific controllers, templates, customized directives and custom filters. 
? Used AngularJS components like controllers, directives, factory and service resources, routing, Dependency injection, 2-way data binding, filters and events. 
? Design and Coding of various Java (1.8), J2EE modules like Spring MVC, Spring Rest, Hibernate. 
? Designed, developed and implemented the business logic required for Security presentation controller. 
? Developed MicroServices to provide RESTful API utilizing Spring Boot with Spring MVC. 
? Consumed web services from other vendors to retrieve information using Spring Rest Client. 
? Involved in writing client-side validations using JavaScript. 
? Used Web services (SOAP) for transmission of large blocks of XML data over HTTP. 
? Developed Data Access Objects to access middleware Web Services. 
? Designed various tables required for the project in Oracle 11g database and used the stored procedures in the application. 
? Wrote Stored Procedures, Triggers and Functions on PL/SQL on Oracle to support both databases for the application. 
? Developed Spring MVC controllers to handle the requests coming from UI. 
? Deployed the application to web sphere application server using WAS admin console. 
? Developed unit test cases using Jasmine and Karma as unit test runner for various application modules. 
? Used GIT for source code maintenance and for version control. Written Maven Scripts to build generate WAR file, used Jenkins for CI/CD process of the application. 
? Interacted with Business Analysts to come up with better implementation designs for the application. 
 
Environment: Java, J2EE, Hibernate, Spring, Web Services, SOAP, REST, XML, XSLT, HTML, AngularJS, Bootstrap, JavaScript, AJAX, Microservices, Groovy, JQuery, Oracle, SQL, PL/SQL, SQL Developer, Git, LINUX, Eclipse, SCRUM.

Java Developer

Creative Hands HR Consultancy Pvt. Ltd
June 2014 to April 2015

Responsibilities: 
? Involved in client-side tooling and testing & development with HTML, JavaScript etc., involved in development, design and implementation of front-end part widget based application. 
? Developed the GUI using Struts Framework, JSP, Servlets, AngularJS, Bootstrap, HTML and server-Side using JSP/ EJB/ JDBC/ SQL. Created various JSPs for presentation layer. 
? The front-end JSP pages were developed using the Struts framework, and were hosted in a J2EE environment on an Apache tomcat server. 
? Configured struts-config.xml and web.xml and properties file provided by Struts framework for the implemented modules. 
? Developed using simple Struts validation for validation of user input as per the business logic and initial data loading. 
? Developed complex, useable, attractive and cross-browser web interfaces that account for speed, file size, readability and accessibility. 
? Created and maintained the framework and layout of each portal with Cascading Style Sheets (CSS), Used the JavaScript and jQuery in the development of the web applications. 
? Involved in Enhancement of existing application utilizing JSP, Created HTML navigation menu that is role based menu items changes dynamically, derived from the database in the form of XML. 
? Successfully implemented Auto Complete/Auto Suggest functionality using AJAX, JQUERY, DHTML, Web Service call and JSON. 
? Extensively participated in developing the website across different browsers and performed cross-browser testing. 
? Documented how Spring Batch is useful for the current project, tried to address the advantages of using Spring Batch 
? Developed applications using Web services, Developed the application using Spring MVC, involved in configuring and deploying the application using Web Sphere. 
? Involved in integrating the business layer with DAO layer using ORM tool Hibernate, transaction Management using the Hibernate configurations. 
? Developed programs to port the XML data to database to make the website XML driven, involved Finding out the bugs, broken links and missing images etc. and rectifying them. 
? Involved in configuring the GIT repository and maintain the version control using GIT. 
? Discussed various ideas/suggestions for the ongoing web sites regarding the page layout and creative design. 
 
Environment: Object Oriented JavaScript, jQuery, AngularJS, Bootstrap, Struts, Spring, Hibernate, Web Services, JSON, AJAX, HTML, XML, DHTML, CSS, JSP, Agile methodology, MySQL, Windows.

Java Developer

CoArtha Techno solutions Pvt. Ltd
July 2013 to May 2014

Responsibilities 
? Involved in Analysis and Design of the Project, which is based on MVC (Model-View-Controller) Architecture and Design Patterns. 
? Involved in developing prototypes of the product. 
? Optimized the code and database for maximum performance Designed Enterprise Application using MVC architecture. 
? Created UML Use Cases, Sequence diagrams, Class diagrams and Page flow diagrams using Rational Rose. 
? Designed and developed UI using HTML, AngularJS, Bootstrap, JSP and Struts where users have all the items listed for auctions. 
? Developed ANT scripts for builds and deployments. 
? Developed Controller Servlets, Action and Form objects for process of interacting with Oracle database and retrieving dynamic data. 
? Day wise transactions were listed in the browser using Servlets. 
? Code complex SQL Join queries for reporting which include hundreds of different variations of reports Responsible for coding SQL Statements and Stored procedures for back end communication using JDBC. 
? Parsing of XML documents was done using SAX and DOM and XML transformation was done using XSLT. 
? Wrote JavaScript validations on the client side. 
? Involved in writing Detail Design Documents with UML Specifications. 
? Responsible for packaging and deploying components in to the Tomcat. 
? Used java IO to create reports, which were mailed to be mailed to manager every hour. 
 
Environment: Core Java, Java Beans, JSP, Jbuilder, AngularJS, Bootstrap, JavaScript, Servlets, JDBC, LOG4J, XML, XSLT, HTML, Struts, Tiles, SAX, DOM, Tomcat, UNIX, Oracle.

Java Developer

Cibersites India Pvt. Ltd
March 2012 to June 2013

Responsibilities: 
? Involved in client requirement gathering, analysis & application design. 
? Involved in the implementation of design using vital phases of the Software development life cycle (SDLC) that includes Development, Testing, Implementation and Maintenance Support in WATER FALL methodology. 
? Developed the UI layer with JSP, HTML, AngularJS, Bootstrap, CSS and JavaScript. 
? Used JavaScript to perform client side validations. 
? Developed server side presentation layer using Struts MVC Framework. 
? Developed Action classes, Action Forms and Struts Configuration file to handle required UI actions and JSPs for Views. 
? Developed batch job using EJB scheduling and leveraged container managed transactions for highly transactions. 
? Used various Core Java concepts such as Multi-Threading, Exception Handling, Collection APIs, Garbage collections for dynamic memory allocation to implement various features and enhancements. 
? Used JPA and JDBC in the persistence layer to persist the data to the DB2 database. 
? Created and written SQL queries, tables, triggers, views and PL/SQL procedures to retrieve and persist the data to the database. 
? Performance Tuning and Optimization with Java Performance Analysis Tool. 
? Implemented JUnit test cases for Struts/Spring components. 
? JUnit is used to perform the Unit Test Cases. 
? Used Eclipse and worked on installing and configuring JBOSS. 
? Made use of CVS for checkout and check in operations. 
? Worked with production support team in debugging and fixing various production issues. 
 
Environment: Java, JSP, HTML, AngularJS, Bootstrap, CSS, JavaScript, EJB, Struts, JDBC, JPA, SQL, DB2, JUnit, JBOSS, Eclipse, CVS.

Skills
AngularJS (5 years), Bootstrap (5 years), data base (5 years), Java (5 years), JavaScript (5 years)
Additional Information
Technical Skills: 
J2SE Technology: Multithreading, Reflections, Collections, Serialization, Networking, Beans 
Middleware Technology: JMS, MQ Messaging, Tibco, Solace, Active MQ 
J2EE Technology: Servlets, JSP, Tag Libraries, JDBC, JNDI 
Architecture & Framework: MVC, Struts, Spring, Hibernate, OSGI 
Database: Oracle 10g, 11g, 12c, SQL server [�] MySQL 
IDE: Eclipse, My Eclipse, NetBeans 
 
Operating Systems: Windows, Linux, UNIX 
Web/App Servers: WebLogic, Web Sphere, Tomcat, JBoss, Apache 
GUI: HTML5, XML, XSD, AJAX, JavaScript, Node.js, AngularJS, Bootstrap, Jquery, 
CSS3, Microservices, Groovy 
Query Languages: SQL, PL/SQL 
Programming Language: C, C++, Java, J2EE 
Design patterns Design Patterns: MVC3, Business Delegate, Business Object, Value Object, Front Controller, 
Database Access Object, Factory, Singleton, Session Facade. 
Tools Tools: Maven, ANT, Log4J, JUnit, TOAD, SOAPUI 4.0.5, JUnit, Rational Rose, 
Visio