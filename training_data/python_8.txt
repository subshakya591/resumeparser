Python Developer Intern
Python Developer Intern
Arlington, TX

� 2+ years of experience in Java, Python, HTML, CSS, JavaScript. 
� Extensive knowledge of J2EE, XML, JSON, REST, EJB, JSTL, JavaBeans and XML. 
� Experience in all phases of SDLC and good understanding of Scrum and Agile Unified process. 
� Knowledge in developing enterprise and web-based applications using JSP, Servlets, Spring, Hibernate. 
� Expertise in DB design, DB normalization and writing SQL queries & PL/SQL. 
� Good understanding of Design Patterns- Singleton, Module, Factory, etc. 
� Experience in building, exposing and consuming (SOAP/Restful) web services in the Business Object layer. 
� Developing automated process for builds and deployments: Jenkins, Maven etc. 
� Experience in writing unit test cases using JUNIT framework. 
� Experience in No-SQL database-MongoDB and SQL databases- Oracle 11g/10g, MySQL. 
� Knowledge of OOP, OOD methodologies and Data structures. 
� Experience in working with Unix/Linux based and Windows environments. 
� Excellent communication and time management skills.

Authorized to work in the US for any employer

Work Experience
Python Developer Intern

Iconix Labs Inc
August 2017 to December 2017

Job Role 
� Developed applications using Python and MySQL for database design. 
� Designed data visualization to present current impact and growth. 
� Visualized annual revenues through Waterfall Chart Model and took part in data extraction, analytic initiatives. 
� Was part of product review, future analysis using Machine Learning models and performed sentimental, 
gender analysis on product advertisements in social media. 
� Developed Natural Language Processing and Machine Learning system.

Software Developer

Neudesic Technologies - Hyderabad, Telangana
January 2015 to December 2015

Job Role 
� Designed the web-tier using HTML, JSP's, Servlets, Spring framework. 
� Involved in the development of business module applications using various J2EE technologies. 
� Used the light weight container of the Spring Frame work to provide architectural flexibility for IOC. 
� Hibernate framework is used in persistence layer for mapping an object-oriented domain model to Oracle. 
� Designed and developed data access layer using DAO, Session Facade, Singleton design patterns. 
� Created JDBC connections for DB connectivity and Maven to automate dependency and customize workflow. 
� Deployed applications on Jboss Application Server and configured database connection. 
� Implemented business rules by writing Stored procedures, triggers and performed unit testing using Junit 
� Worked on projects using singleton and abstract factory patterns over MVC architecture.

Web Developer Intern

BNew Mobiles Pvt Ltd - Hyderabad, Telangana
January 2014 to May 2014

Job Role 
� Co-developed user interactive web pages using technologies - HTML, CSS, Bootstrap, JavaScript and JQuery. 
� Handled aspects of the UI development including analyzing, designing and testing web applications. 
� Used SEO practices to elevate web presence and user experience. 
� Designed dynamic client-side JavaScript codes to build web forms to simulate form validation using jQuery and JavaScript. Used jQuery UI and plugins to enhance the functionality and user experience for website. 
� Co-developed websites with cross browser compatibility features using CSS and resolved multiple CSS 
compatibility issues. Worked closely with senior developer and learned project management skills. 
 
RESEARCH & PROJECTS 
Python Data Science (Technologies used: Python, TinyDB, Tableau) 
� Developed a python script to exact all the places and their descriptions from a source. 
� Stored the data in a JSON file using TinyDB. Mined patterns in the data to generate deep insights for data gaps. 
� Performed Sentiment and Gender analysis on various texts of Mark Twain using RapidMiner, Python. 
� Increased analysis efficiency by developing dashboards using Tableau, visualizing reports and types of data store 
� Examined descriptions of individual locations, federal census data and election returns by using text mining. 
 
Java RESTful API (Technologies used: REST API, JAVA API (JAX-RS)) 
� Designed a messenger application that shows the implementation of RESTful API using JAX-RS (Jersey) and deployed on Tomcat server. A user could perform CRUD operations through this application. 
� Needed a Rest Client such as RESTED or POSTMAN to check the functionalities of the application. 
 
Proxy Remedy (Technologies used: JAVA, Eclipse IDE, JDK and Android SDK) 
� Developed an android application which can be used by a person to know the generic equivalent of a drug and the availability of the drug in the nearest store which uses a Google API implementation. 
� The application is designed with a huge database of 100,000 medicines where optimizations had to be done to reduce the response time. 
 
Sales Order (Technologies used: REST API, MySQL, HTML5, CSS3, PHP, JavaScript) 
� Designed and developed a retail store platform by Restful web API using PHP, JavaScript framework with 
MySQL at the backend. 
� The system was designed to support dynamic products database incorporated into it and actions such as 
Admin Operations, Customer purchases, Warehouse operations and view products. A third party secure 
payment interface is also included. 
 
E-grocery store Database (Technologies used: MySQL, Microsoft SQL Server Management studio) 
� Built the complete database which contained data on various grocery items available, employee information, 
customer information and their payments using Microsoft SQL Server Management studio 2012. 
� Created an ER model for online grocery store and used it to build the database, populate it. 
� Tested the DB by writing a set of queries in SQL to extract the data from the database.

Education
MS in Computer Science

Northern Illinois University DeKalb, IL
December 2017

Bachelor of Technology in Computer Science

Mahatma Gandhi Institute of technology Hyderabad, Telangana
May 2015

Skills
APPLICATION SERVER (Less than 1 year), J2EE (Less than 1 year), JBOSS (Less than 1 year), JDBC (Less than 1 year), JUNIT (Less than 1 year)
Links
http://www.linkedin.com/in/vikranthaddanki

Additional Information
SKILLS 
 
� Languages & Frameworks C, C++, Java, Python, MySQL, PL/SQL. 
� J2EE Servlets, JSP/JSTL, JDBC, JMS, EJB. 
� Web Technologies HTML5, CSS3, PHP, JavaScript, AngularJS, XML, XSTL, WSDL, RESTful API's. 
� Databases Oracle, DB2, Microsoft SQL Server, SQLite, TinyDB, PostgreSQL, MongoDB. 
� Frameworks Spring, Hibernate, Asp.net, Junit, Maven 
� Tools Visual Studio, Eclipse IDE, Android Studio, NetBeans, MySQL workbench. 
� Application Server Tomcat, Websphere, JBoss