Data Scientist
Data Science, Statistical Modeling and Reporting, Predictive Analytics
Dallas, TX

I am skilled at the field of data science, especially in quantitative modeling. I am familiar with current popular statistical software: SAS,SPSS, EViews and open software tools like Python-based Pandas, Sci-Py, and Num-Py to do data manipulating, modeling, and predicting. I would like to work in data science or related fields.

Authorized to work in the US for any employer

Work Experience
Data Scientist

Independent Contract - Dallas-Fort Worth, TX
October 2017 to Present

Performed exploratory research for Data Science.  
Tested innovative potential applications outside of mainstream usage. 
Compared hyperparameters� tuning and predictive models� results among different statistical softwares.

Data Analyst

WageWorks - Irving, TX
June 2017 to September 2017

Assessed client and system issues including technical and application quickly and handled with error and exception in order to maintain data quality. 
Analyzed and monitored data updates to ensure data integrity with system application and used SQL scripts to execute successfully or resolve users� problems and data patch. 
Processed (including load/test/process) inbound and outbound files in accordance with established Service Level Agreements (SLA�s). 
Technology: Microsoft SQL Server Management Studio, Power Query of MS Excel, internal production support system.

Data Analyst � Business System

Kintetsu Global I.T., Inc. - Dallas-Fort Worth, TX
December 2016 to May 2017

Reviewed, analyzed, and evaluated global logistic business systems and user needs. 
Tested data mapping with Python for methods and implementations. 
Tested reporting functions to make sure SAP Crystal Report output correctly and prepared tested reports for the data verification between new version and old version. 
Performed PL/SQL scripts like merge, join tables to do debugging to support the enterprise systems and applications in department of system development and production support in large amounts of data.  
Completed about 100 tickets for production support, tested and processed 30 reports (including multinational technology companies) and 20 new functionality of the new system development and web application. 
Resolved users� problems by dealing with data patch (data correction) and work-around application and providing advanced instruction and documentation.

Data Analyst - Compliance

Genpact - Richardson, TX
May 2016 to November 2016

Applied broad compliance knowledge to develop KYC/AML operations such as data enrichment and data remediation for top 5 banking client.  
Performed data enrichment by confirming data meets or exceeds all minimum compliance requirements including searching and saving relevant PDF files for fraud prevention by using internal and external databases. 
Performed data Remediation through research and analysis of required supplementary information and prepared daily Excel reports. 
Processed about 1500 cases of KYC/AML in total during that time.  
Prepared KYC/AML validation for different data types (geographic data, third-party data and customer information data).

Operation Analyst

Dimerco Express (U.S.A.) Corp - Grapevine, TX
June 2014 to March 2016

July 2014-May 2015 
Operated air logistics, especially international trades to Asia, including global semiconductor companies. Prepared PDF files from clients to do operations for international shipments. 
Applied cloud concepts to collaborate with all users in a faster and more efficient way and prepared daily Excel reports like Tableau.  
Prepare and tested data requirements for business intelligence and analytics solutions.

Education
Master of Science in Information Technology and Management

The University of Texas at Dallas Dallas, TX
May 2015

Master of Arts in Political Economy

National Cheng Kung University
January 2008

Bachelor of Arts in Business Administration

National Sun Yat-sen University
June 2005

Skills
Business Intelligence Software-SAS, SAP (3 years), Database: Oracle, My SQL, PL/SQL, MS SQL(SSMS) (2 years), HTML, CSS, JavaScripts, Java, Perl, Linux shell script (1 year), Unix System(OS X) (8 years), MS Office, Statistical Software-SPSS, EViews (10+ years)
Links
http://datadective.blogspot.com/

Awards
Honor Roll, NSYSU, Taiwan

Fall 2001, Spring 2001, Fall 2002

Merit Scholarship Awards, NSYSU, Taiwan

September 2001

for 4 consecutive Semesters since 2001

Distinguished Academic Performance, Department of Political Economy, NSYSU, Taiwan

June 2005

Graduated with Honors, NSYSU, Taiwan

June 2005

Certifications/Licenses
SAS Certificate in Business Intelligence and Data Mining

August 2014 to Present

Issued by the SAS Institute and UT Dallas

SAS Certified Base Programmer for SAS 9

November 2016 to Present

Issued by SAS

Certificate of Completion the course, Data Science

November 2015 to Present

Issued by Divergence Academy

Additional Information
Languages: Mandarin Chinese and English