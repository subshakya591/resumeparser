Full Stack Python Developer
Full Stack Python Developer - SiSTeR Technologies
Plano, TX

Seeking full-time position as a software development engineer.

Authorized to work in the US for any employer

Work Experience
Full Stack Python Developer

SiSTeR Technologies
August 2017 to Present

* Develop Python core libraries, Django REST APIs which perform the data transfer from mobile phone to ElasticSearch database and Amazon S3 (Simple Storage Service). 
* Develop AngularJS website from front-end to back-end. (HTML5, CSS, JQuery, JavaScript). 
* Manage and maintain web servers on EC2 (Linux, Nginx, Gunicorn, Supervisord).

Senior Software Developer

Fireracker
June 2016 to July 2017

* Established Django framework website from front-end to back-end. 
* Designed database, collected and analyzed data. (Selenium, MSSQL, Stored Procedure)

SQA Specialist

Symantec Chengdu R&D Center
December 2012 to April 2015

* Developed PHP Symfony website from front-end to back-end and REST APIs, desktop application tools. 
* Developed automation test cases based on Selenium library, tracked and reported test results. 
* Designed database, collected and analyzed data. 
* Administered Jenkins server, Python trainer. 
* Led a team in an agile project. 
* Demonstrated a continuous integration development processing on Cutting Edge 2014.

Software Developer

Tieto Information Technology
January 2011 to December 2012

* Developed Python core libraries, automation test cases for Java desktop application, QT desktop application and mobile phone application with robot framework and Jenkins. 
* Implemented QT mobile phone application. 
* Reported tasks status on JIRA and in agile scrum meetings.

Software Developer

Expacta Digital Technology
March 2010 to January 2011

* Worked as team leader. 
* Developed and maintained website from front-end to back-end. (Apache)

Education
Masters in Course of study

University of Texas at Dallas Dallas, TX
August 2017 to Present

Bachelors in Course of Study

Chengdu College of University
September 2006 to July 2010

Skills
PYTHON (6 years), REST (2 years), PHP (4 years), JAVA (2 years), ROBOT (2 years)
Links
https://github.com/Chenjinyu/

https://www.linkedin.com/in/jinyu-chen-47836698/

Additional Information
8+ years as a software developer with hands-on experience in all levels of requirement, performance and risk analysis, platform structure, database, function, testing case design and implementation, risk management. 
Outstanding interpersonal skills, an enthusiastic team player, an efficient leader, a creative thinker. 
 
TECHNICAL SKILLS 
Python, PHP, Ruby, C#, JAVA. HTML5, CSS, Bootstrap, JavaScript, JQuery. MSSQL, MYSQL, Elastic Search. 
Django, Django REST, AngularJS, Symfony Framework, Cucumber, Robot Framework, Jenkins (Hudson), Agile Scrum, AWS (Amazon Web Service), Selenium. 
GIT, SVN, Perforce, SSH. 
Eclipse, Netbeans, Visual Studio, JIRA, PHPMyAdmin, SQL Server Management Studio, QT Creator.