Sr. Java Developer
Sr. Java Developer - Lucent Technologies, New Jersey
Dallas, TX

� 9+ years of professional experience in Requirements Analysis, Design, Development and Implementation of Java, J2EE and client-serve r technologies. 
� Involved in all of Software Development Life Cycle (SDLC) in large scale enterprise software using Object Oriented Analysis and Design. 
� Good experience in Agile and SCRUM methodologies. 
� Extensive experience in developing applications using Java, JSP, Servlets, JavaBeans, JSTL, JSP Custom Tag Libraries, JDBC, JNDI, SQL, AJAX, JavaScript and XML. 
� Strong Working experience in Design, Development and implementation of several J2EE frameworks like (Model View Controller) Struts, Spring, Hibernate and EJB 
� Strong front-end development using Adobe Flex, MXML, Action Script, AJAX, Java Scripts, CSS, HTML and DHTML. 
� Expertise in developing Flex Rich Internet Applications (RIA) with Cairngorm MVC architecture and Mate Framework, FlexUnit for testing , BlazeDs for remote development services, Action Script 
� Good experience in developing Flex Custom Components, Flex Data services, Charting, Error handling, Advanced graphing, Dashboard layout design, analyzing designing and deploying a Flex applications. 
� Implemented Design patterns such as MVC, View Dispatcher, Data Access Objects, Singleton, Observer, Factory, Session Facade. 
� Experience in configuring and deploying Web Applications using Web Logic, WebSphere, JBoss, Tomcat and Apache. 
� Expertise in using IDE's like RAD, IBM WebSphere Studio Application Developer (WSAD) and Eclipse Used Hibernate framework as persistence layer 
� Used Spring framework to autowire/inject components and also configure the batch jobs 
� Thorough knowledge of IDE like RAD, WSAD and Eclipse. 
� Developed ANT scripts for the build and deployment of J2EE applications. 
� Design, analysis and modeling of n-tired, Object-Oriented system architectures using Rational Rose. 
� Thorough knowledge in using UML, XML parsers (SAX, DOM), SOAP, HTML and DHTML. 
� Sound RDBMS concepts and extensively worked with Oracle, PL/SQL, SQLServer & DB2. 
� Familiar with designing and coding Stored Procedures, Triggers, Cursers and Functions using PL/SQL. 
� Very good experience in developing Test Cases using JUnit. 
� Experienced in using CruiseControl for Continuous Integration. 
� Used Hibernate as an Object relational mapping (ORM) library. 
� Experience in Internationalization (i18n) and Localization (l10n). 
� Experience in XML, WebServices using SOAP, XML-RPC, JAX-RPC, JAXB and Messaging standards like JMS.

Authorized to work in the US for any employer

Work Experience
Sr. Java Developer

Lucent Technologies, New Jersey
April 2016 to Present

Description: Lucent Technologies, Inc was an American multinational telecommunications equipment company. I have designed and developed applications for Lucent platform using Java/J2EE, struts, spring, hibernate, web services and jRules. 
 
Responsibilities: 
� Defined and constructed layers, server-side objects and client-side interfaces based on J2EE design patterns, Developed and implemented Swing, spring and J2EE based MVC (Model-View-Controller) framework for the application 
� Co-developed dynamic Ruby on Rails/HTML5 application highlighting numerous data visualization of web metrics. 
� Built and maintained several servers and workstations including Windows Server and desktop variants, Ubuntu Server and desktop variants, Fedora, and CentOS. 
� Managed source control on a day to day basis in GitHub 
� Used GruntJS, BowerJS, Express from NodeJS. 
� Coded scripts to clean normalize, reformat data for loading into the ERP system using VB Script and Ruby scripts 
� Developed and implemented many PhoneGap and Titanium applications along with Java and Objective-C plug-ins for iPad, iPhone and Android platforms. 
� Environment was mixed Windows and Linux, projects written in C and Java. 
� Architected a customized network protocol used by client applications including Android, iOS and Windows Phone 7 to communicate with cloud architectures 
� Skilled in taking the support of Cordova for deploying application into Mobiles with OS like Android and IOS. 
� Worked with deployments on Ant, Maven, Gradle and Deployment scripts using Shell Scripts 
� JIRA & Confluence were the tools used to keep things in check. 
� Developed the Web Based Rich Internet Application (RIA) using J2EE (Spring framework) and Macromedia/Adobe Flex. 
� Implemented Persistence layer using Hibernate to interact with the Oracle database, Used Hibernate Framework for object relational mapping and persistence. 
� Develop cross browser compatible web pages quickly and effectively using jQuery, HTML5, CSS3, Responsive Web Design, Bootstrap and Angular JS. 
� Implemented i18n (internationalization) for UI to support multi-language feature, Dashboards with graphical charts using Angular JS. 
� Designed and developed Enterprise Eligibility business objects and domain objects with Object Relational Mapping framework such as Hibernate. 
� Implemented MVC architecture using Jakarta Struts framework, Worked onSpring/HibernateApplications 
� Writing Back end scripts on Node.js/JavaScript. 
� Developed web applications using Spring MVC, JQuery, HTML, Bootstrap for Virtual Infotech Inc. 
� Worked on the modernization of a legacy and outsourced UI. Technologies used were Backbone.js, Can.js, and JQuery 
� Extensively used Oracle programming using SQL and PL/SQL 
� Developed a bulletin board module for supplier and vendor information portal. Application included forms to upload notification documents to the Supplier bulletin and maintenance of documents, complete with CRUD capabilities and email notifications. 
� Developed Oracle PL/SQL Stored Procedures and Queries for Payment release process and authorization process. 
� Connectivity to the database is provided using JDBC. 
� Used Oracle coherence to find POJO beans from the deployed WAR file. 
� Created distributed Oracle coherence domains, configuration and system design. 
� Built a RESTful API to save and retrieve geolocations using a remote server in Java using Spring, MongoDB, Apache CXF, and JAX-RS 
� Technology leadership for the direction of the Cloud based application using Node.JS, PDFJS, Sencha, MongoDB and MS SQL Server integrating to a proprietary framework. 
� Worked on web services SOAP and REST technologies 
� Involved in debugging the product using Eclipse and JIRA Bug Tracking 
� Implemented multiple J2EE web service projects using Java, REST, SOAP, Spring, WebLogic and Websphere in production. 
� After fixing defects used SVN (Apache Sub versioning software) to check in the changes. 
� Involved JUnit for the back-end Unit Test, Used Flex Unit for the Flex test. Assisted the testing team for Black-box and White-Box testing 
� Followed an Agile, Software Development methodology. Used NetBeans IDE 
 
Environment: Java, J2EE, Spring, Hibernate, Struts, Ubuntu, GitHub, GruntJS, Ruby, Objective-C , C, Ruby on Rails, iOS SDK, Android SDK, Cordova, Gradle, Jira, JQuery, AJAX, Sencha ExtJS, JavaScript, Oracle, Crud, PL/SQL, JDBC, Apache CXF, Rest, Eclipse, Weblogic, ClearCase, Junit, Agile, UML JSP, JSTL , Servlet, Maven, IText, GWT, Jasper report, ILOG, Web 2.0, SOA.

Sr. Java Developer

OCI Chemical Corp
October 2014 to April 2016

Description: OCI is a petro and coal chemicals, inorganic chemicals, and renewable energy products company. The project mainly focuses on converting the data from database to XML feeds by building a java process to the clients. 
 
Responsibilities: 
� Developed the Web Based Rich Internet Application (RIA) using J2EE (Spring framework) and Macromedia/Adobe Flex. 
� Implemented application level persistence using Hibernate and Spring. 
� Created development build and deployment process using GulpJS to work with TFS based deployment process. 
� Development mobile web application for Android, iOS and BlackBerry Device 
� Using Ruby on Rails for development with backbone.js 
� Develop the codes in C programming language 
� Created the app for the iPhone and iPod touch using the iPhone SDK with Cocoa Touch and Xcode to develop Objective-C code. 
� coded scripts to automate various manual processes (Screen Scraping) using VB Script and Ruby 
� Deployed the web-based application into Android OS using Cordova's support. 
� Helped to deploy and resolve issues during deployment and Configured Maven, Gradle, Shell Scripts. Used Jenkins for Continuous Integration (CI) and Hudson. 
� Using JIRA to manage the issues/project work flow 
� Configuring Hibernate, Struts and Tiles related XML files. 
� Developed user interfaces using JSP, JSF frame work with AJAX, Java Script, HTML, DHTML, and CSS. 
� Designed and Developed the UI screensusing JavaScript, Angular JS, JQuery, JSP, requireJS, Html and CSS. 
� Implemented the search functionality for Internal Application using Angular JS and Node js. 
� Implemented User interface(UI) entire application using JSP, JSTL, Custom Tag Libraries, JavaScript, XML/ XSLT, HTML and CSS 
� Developed GUI screens for XTT & LD application using JSP, HTML and CSS 
� Used Ajax calls extensively - using Jquery, inside the WebApps and create a seamless flow of Quotes. 
� Was involved in UI development, and Developed presentation layer using JSP, HTML5, DHTML, CSS, AJAX, JavaScript, JSTL Taglibs. 
� Software is built as a web app on Node.js with Backbone.js. CSS is compiled with LESS and HTML template engine is Jade. 
� Developed web applications using Spring MVC, JQuery, HTML5, Bootstrap for Virtual Infotech Inc. 
� Developed UI components for email and link sharing of documents and files for a Content Management System using Backbone.js and jQuery 
� Wrote Java programs, performed builds, monitored DB2 data and ran SQLs. 
� Providing support & development of web based applications and TCP/IP socket applications in UNIX, using PHP, C, Perl, JavaScript, Oracle, MS SQL, t-sql, Pl/SQL, openSSL. 
� Proven expertise in implementing IOC and Dependency Injection features in various aspects of Spring Framework (Core, Web, JDBC, MVC and DAO). 
� Worked on Java Multithreading, Collections & Coherence Framework 
� Extended pagination, profiling functions and coherence cache support for existing RESTful service 
� Strong real time experience in Java technology and Spring MVC, Spring IOC, Hibernate, JavaScript, JQuery, AngularJS, HTML, CSS, MongoDB. 
� Improved data analysis efficiency by adding 30 new features using Java 8 (Spring, JAX-RS) and MongoDB 
� Designed and Developed web services using JAX-WS, SOAP, JAXB, ApacheCXF. 
� Used Web Services for creating rate summary and used WSDL and SOAP messages for getting insurance plans from different module and used XML parsers for data retrieval 
� Development and deployment of ear, creation of Datasources, Webservices on Weblogic server. 
� Used CVS to maintain source and version management and used Eclipse as IDE. 
� Experience working on Jquery , agile agile, WebSphere Portal User management APIs 
� Implemented improvements to the WebSphere Commerce Organization Console and the Administration Console to display, and allow for update, new member information. 
� Extensively used IBM WebSphere Integration Developer(WID) to implement SOA 
� Used WSAD for writing code for JSP, Servlets, and Struts 
� Design and implementation of desktop, mobile and web applications in Java and HTML5/Javascript using Eclipse, NetBeans, and Xcode IDE's; Swing, GWT, Vaadin, JPA, Hibernate, PhoneGap frameworks; and SVN, git, maven source code environments. 
� Led development team using J2EE creating web enabled invoicing system for multiple location service company in NetBeans environment using Git-Hub version control. 
� Wrote test cases using JUnit testing framework and configured applications on Weblogic Server 
� Followed Agile and SCRUM in development process 
� Optimized UML design using patterns like DAO, Factory, Service Locator and Business Delegate 
� Used C++, Java, Servlets, JDBC, JSP and DAO for Coding/Implementation. 
Environment: Java, J2ee, Spring, Hibernate, Struts, Ruby, Objective-C , C, Ruby on Rails, iOS SDK, Android SDK, Cordova, Gradle, Jira, GulpJs, AJAX, JSF, EXTJS, JavaScript, Dojo, CSS, HTML5, CRUD, PL/SQL, JDBC, DB2, Apache CXF , SOAP, Web Services, Eclipse, WebSphere Portal, Subversion, Junit, Scrum, UML, JSP, JSTL, Linux

Sr. Java Developer

Target Corporation
October 2013 to October 2014

Description: Target Corporation is the second-largest discount store retailer in the United States. Created Web pages and integrated with backend and database. The front-end is developed with Angular JS framework and backend with JAVA and SQL database. 
 
Responsibilities: 
� Configured Struts, Hibernate framework with Spring MVC 
� With the IBatis implementation, all the queries were separated away from the code and moved to the XML files. Used Spring IBatis XML configuration files to call the queries. 
� Performed the validations using Struts and JPA. 
� Developed user interfaces using JSP, JSF frame work with AJAX, Java Script, HTML, DHTML, and CSS. 
� Designed and implemented UI layer using JSP, JavaScript, HTML, DHTML, JSON, XML, XHTML, XSL, XSLT, XSL-FO and business logic using Servlets, JSP, EJBs and J2EE framework. 
� Developed the UI panels using JSF, XHTML, CSS, DOJO and JQuery 
� Integrate the ExtJs/Sencha framework with Oracle BPM. 
� Enhanced the functionality of screens using JSP, HTML, JavaScript, CSS and JQuery. 
� Set up JBoss Server, Configured MySQL Cluster in Linux OS and installed OpenCall XDMS 
� Used TOAD for all database query testing and optimizations 
� Scheduled jobs, Alerts using SQL Server Agent. 
� Involved in configuring JNDI properties, data sources for Oracle, UDB, DB2 data bases in websphere. 
� Developed web services using Apache Axis2 in Java and SOAP/WSDL on SOA architecture 
� Developed Web services using Apache CXF and exposed WSDL for Flex client to consume 
� Creates GUI builder using NetBeans. 
� Implemented JBOSS Cache provider with Hibernate second-level cache for clustered caching using cache factory interfaces and also defined configuration mappings like setting the Isolation levels and other cluster configurations. 
� Create and maintain Amazon EC2 instance that supports the companies Redmine project management site and Mercurial CM. 
� Responsible for ClearCase administrative tasks (working with branching/merging, triggers, labeling etc). 
� Review Requirement, develop technical design documents and create a prototype of the critical business application using JAVA/J2EEInitiated use of httpUnit, Selenium IDE for testing. 
� Executed project using waterfall model and practiced Agile Project Management & performed the Scrum Master role for specific projects/requirements as appropriate. Used Rally for Agile project management 
� Designed the UML diagrams based on the OOAD principles 
� Developed the Presentation and Controller layers using JSP, HTML, Java Script, Business layer using Spring (IOC, AOP), DTO, JTA, and Persistent layer DAO, Hibernate for all modules. 
� Designed and implemented business functionality for WEB system hidden login authentication, agent import and "can see" using the following technologies: JAXB, SAX, XML, and Servlets. 
� Used ANT for building the application and deployed on BEA WebLogic Application Server. 
� Used SOAP Technology for exchanging the XML application using HTTP 
� Customized the EMC Documentum Webtop to conform the overall portal themes 
� Designed and currently developing 3 Tier Web 2.0 based price management system using GWT, Grails and MySql. Used and using Talend Open Studio to load data into the new system from the legacy data formats. 
� Working with the JIRA tool for QualityCenter bug tracking. 
� Designed and developed front end screens for new reports using Swing components. 
� Designed Power Cubes using Cognos Transformer 
� Involved in ILOG Validation System design decisions and architectural designs with Enterprise architect and other Team members 
� Layout and design the overall architecture and migration approaches using Oracle ADF. 
� Page design and development using Oracle BPM. 
� Used IBM MQ Series in the project. 
� Extensively worked on Application servers like Weblogic, Web Sphere, jboss and Apache Tomcat. 
� Backend development with PHP, MYSQL, BASH, LINUX, UNIX 
� Deployed and tested the application on Weblogic in windows and Unix environment. 
� Implemented MVC design architecture using Ruby on Rails and MySQL 
� Oversaw many conversions to WordPress and Magento 
� Upgraded Drupal websites from version 5 to 6, and 6 to 7 
� Extensive Development using Zend Framework by PHP in a LAMP environment. 
� Reverse engineered and reengineered the Student Enrollment System for the Louisiana Community Technical College System using VB and ASP.NET. 
� Involved in bug fixes of the various components in C++, Perl, shell and Java 
� Porting the entire software to use shared libraries on the Tandem OSS and Linux which involved modification of make files as well as changes to C/C++ code. 
� Developed and implemented Legacy system programs by using COBOL, DB2, CICS, JCL, JAVA and VSAM. 
� Expose PL/I database access layer as web service using RD/Z 
� Provided maintenance programming support for TPS, an estimating and reporting legacy system, using PL/I, FORTRAN, TSO, Dialog Manager, Regional I datasets, SAS, CLIST and JCL. 
� Performed heavy hands-on coding in Java, SQL Stored procedures, COBOL, JCL, and utilized iText java library to convert reports in text format to PDF format. 
� Acting as liaison between management and development team for requirements and QA. 
Technologies/ Tools Used: UNIX Shell Scripting, PERL, SAS/ EG 9.2, BMC Control-M, BMC Remedy, XML, XSLT, Java/ J2EE 
� Reduced customer report generation time by 80% by developing automation tool with VB & SQL within two weeks. 
 
Environment: Spring, Ibatis, Struts, JSF, EJBs, JQuery, MySQL, TOAD, MySQL, DB2, Apache Axis, WSDL, NetBeans, JBoss , CVS, VSS , Selenium, water fall model, UML, JSP, Servlets, ANT, XML, EMC, Jira, Swing, cognos, Oracle ADF , Oracle BPM, IBM MQ, Tomcat Server, Linux, Unix server, ILOG, Ruby, WordPress, Drupal, C, C++

Java Developer

Chicago, IL
April 2011 to October 2013

Project: Early Warning Reports 
 
Description: The online dashboard of Early Warning reports enhance the productivity of its users by significantly reducing their data collection time and allows them to focus on scoping the initiatives. It will also provide a more holistic consolidated view for the comparison of data from multiple data sources. More importantly, the dashboard will provide the ability to easily compare normalized data sources against each other in order to identify which focus areas perform least well on a level playing field. The dashboard will eliminate the need to access disparate data sources and overcome the challenges of standardizing reports from different sources per user. A simple easy to use interface will allow users to access reports and download the same for business improvement activities. 
 
Responsibilities: 
� Agile methodology was adopted in the development, and used Scrum method of project management. 
� Implemented UI layer using Flex and business layer using Spring MVC and developed persistence layer using Hibernate. 
� Designing Flex UI components as well as development of Flex custom components. 
� Developed eye catching MXML view components using Flex Builder. 
� Used Cairngorm MVC Architecture to handle the events and to retrieve the data from the server. 
� Responsible for design and development of user interface using Flex Layout and Navigation, Data grid menus, and skinning components. 
� Used Cisco Cues charts for creating Bar charts, Line charts and Pie Charts to show the project specific performance metrics 
� Integrated Flex with BlazeDS to communicate Server side Objects which was build using Spring Framework and Hibernate. 
� Extensively used RemoteObjects to retrieve data from the remote server and perform required business functionalities from the front end. 
� Used spring framework modules like Core container module, Application context module, Spring AOP module, Spring ORM and Spring MVC module. 
� Configured Spring Application-Context.xml used by spring container to inject dependencies to java classes to optimize memory resources. 
� Implemented Spring IoC (Dependency Injection) and Spring Aspect Oriented Programming (AOP) for the Business as well as Lookup Service Layer development. 
� Developed / modified the model components to incorporate new business level validations. 
� Responsible for the oracle schema design, generating various POJO objects and generating their corresponding Hibernate mappings (.hbm) files. 
� Consumed WebServices for getting the credit card information from third party application. 
� Used Singleton and DAO design pattern. 
� Test Driven development is done by maintaining the Junit and FlexUnit test cases throughout the application. 
� Log4j package is used for the debugging. 
� Used ClearCase for version control. 
� Ensuring adherence to delivery schedules and quality process on projects. 
 
Environment: Java , J2ee, Spring , Hibernate , Flex , Action Script, MXM, XML, XSD, Java script, Blaze DS , Cairngorm MVC Framework, IBM RAD, ClearCase, Oracle, Log4j, Weblogic, Sql, DbVisualizer, Webservices, Agile, ClearQuest, Maven, UML (Rational Rose), HTML , CSS and Windows 2000 Prof

Java/Flex Developer

Fidelity National Finance - Atlanta, GA
April 2009 to April 2011

Project: Recording Fee Calculator 
 
Description: The scope of the Recording Calculator project is to meet the needs of Quality Assurance for recording fees and taxes within the Vision application. Recording fee Calculator allows the user to calculate and save recording fees for an order. This includes support of all FNF Clients, including rates and forms; support of all products, and coverage in all states where the company records deeds and mortgages. This application is built using Flex for UI integration layer with hibernate with Oracle as backend database. This project is implemented using Agile methodology. 
 
Responsibilities: 
� Followed the Agile methodology iterative process for each of the module in the project. 
� Developed the Login Module and Reimbursement Module. 
� All modules developed with the framework design patterns and DAOs, Hibernate for persistence layer, Spring Framework for Application layer. 
� Prototyped Flex based RFC using Flex, consuming web services using BlazeDS. 
� Used Spring framework in enabling Controller Class access to Hibernate. 
� Designed and developed several Flex UI Screens and Custom Components using MXML and Action script. 
� Designed and developed user interface using Flex Components: ViewStack, Checkboxes, Repeater, Title Window, Text Area and others. 
� Created components that have multiple views using States and also used Effects and Transitions when a State is entered or exited. 
� Extensively used to data models to store the form data. 
� Worked on a proof of concept on Internationalization in flex. 
� Developed Action Script functions for event handling response handling and error handling 
� Implemented Singleton Service Locator design patterns in Cairngorm MVC architecture to interact with backend. 
� Implemented JSON with Flex to feed Data grids into the web application. 
� Worked on development of Error Handling mechanism on the Flex side by catching the error 
code and message streamed from server and wrapping it in a custom Exception Object. 
� Used Blaze DS to connect flex application with backend J2EE and Web Services layer to pull out the data to Flex UI components 
� Was responsible for deploying and testing webservices components. 
� Responsible to create Web Services to various web applications using Apache Axis 2 responsible to create WSDL, WSDL2Java and Java2WSDL Stubs 
� Extensively used SOAP in webservices to communicate with other applications. 
� Used SAX and DOM for parsing XML. 
� The Log4j package is used for the debugging. 
� Created JUnit and FlexUnit test case design logic and implemented throughout application. 
� Used ANT tool for building and deploying the application. 
� Used Toad tool to create Views and Stored procedures. 
� Created Data model (Schema) of the Database. 
� The project was implemented in Websphere Application Server for the development and deployment. 
� Participated in regular code reviews and design meetings. 
 
Environment: Java, JSP, J2EE, Hibernate, Log4j, Struts, JSON , Apache Axis 2, Spring, WebServices , Adobe flex builder, Flex API , Blaze DS, Flex, MXML, Action script, Tomcat Web Server, Websphere, IBM RAD V6, ClearCase, JQuery, Agile , VSS, Sql Server, Pl/Sql , TOAD, XML, CruiseControl , Unix, HTML, XSLT and CSS.

Java Developer

Liberty Mutual Insurance - Atlanta, GA
April 2007 to April 2009

Project: Enhanced Commercial Line Policy System 
 
Description: Liberty mutual is a market leader in key insurance and investment product lines. Liberty Mutual continually seeks to better serve its customers by leveraging ongoing product innovation, knowledge and experience, a strong brand, a broad and powerful distribution system. The project deals with building up of a new web-site for Liberty Mutual Agents to assist Liberty Mutual customers. This web-site is used by Underwriters as well. Agents were given access to view their corresponding policy related documents 
 
Responsibilities: 
� Involved in modifying, updating and testing of the component. 
� Involved in group meeting with teammates and made substantial changes to the architecture to improve performance of the Application. 
� Modified JSPs, used struts tag libraries and Java Server Pages Standard Tag Library (JSTL). 
� Maintained Struts architecture throughout the application. 
� Developed the bean classes for better data exchange between the MVC layers 
� Successfully differentiated presentation from code. 
� Used multi threading and socket programming. 
� Deployed the application on WebLogic, Application Server. 
� Created connection pools and data sources. 
� Modified JavaScript to read client side activities (events) and data and event validations. 
� Used WinCVS as version Control system. 
� Suggested ER models and changes in the table structures 
� Modified SQL, PL/SQL procedures and triggers to obtain optimize output. 
� Involved in separation of components and deploying in different servers and balancing the load among each Server. 
� Solved performance issues and brought optimal output from the available resources. 
 
Environment: Java, J2EE, Sybase, BEA WebLogic Server, JSTL, SQL Server, Struts Frame work, Servlets, JSP, EJB, WinCVS, JavaBeans, Eclipse, UML, Windows XP/Linux.

Skills
J2EE (10+ years), JAVA (10+ years), MODEL VIEW CONTROLLER (10+ years), MODEL-VIEW-CONTROLLER (10+ years), MVC (10+ years)
Additional Information
Technical Skill: 
 
J2EE Technologies: JSP, Servlets, JavaBeans, Struts, JDBC, JSP Custom Tag Libraries, XML (SAX & DOM), JNDI, JMS, Applets, Log4J, JSTL, JUnit. 
Middleware: ODBC, JDBC, RMI, Blaze DS. 
Web/App Servers: IBM WebSphere, Tomcat, JBoss, Apache, Resin, Apache Axis. 
IDE Tools: EditPlus, TextPad, Eclipse, WSAD, RAD 
Languages: Core Java, using generics, JavaScript, ActiveX, Perl, PL/SQL, XML, 
Frameworks: Hibernate, Spring, Spring MVC, Mate, Adobe Flex, MXML, Action Script Rational Rose, UML, JUnit, Ant, Struts, Ajax, DOJO. 
 
Methodologies: Agile, SCRUM, TDD, Design Patterns, Continuous Integration using CruiseControl