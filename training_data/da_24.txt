Data Scientist
Data Scientist - OnBlick Inc
 Over 6+ years of IT experience with 4 plus years of experience on Statistics, Data Analysis, Machine Learning using Python. 
 Experience as a Data Scientist architecting and building Data Science solutions using Machine Learning, Statistical Modeling, Data Mining, Natural Language Processing(NLP) and Data Visualization 
 Ability to analyse most complex projects at various levels Experience in building Big Data data-intense applications and products using open source frameworks like Hadoop, Pig, HIVE, Sqoop, Apache spark, Apache Kafka, Storm and Apache Mahout. 
 The experience of working in text understanding, classification, pattern recognition, recommendation systems, targeting systems and ranking systems using Python. 
 A deep understanding of Statistical Modelling, Multivariate Analysis, Big data analytics and Standard Procedures Highly efficient in Dimensionality Reduction methods such as PCA (Principal component Analysis), Factor Analysis etc. Implemented machine learning algorithms such as Random Forests (Classification), K-Means Clustering, KNN (K-nearest neighbors), Naοve Bayes, SVM (Support vector Machines), Decision Tree, Linear and Logistic Regression Methods. 
 Worked with applications like R, SPSS and Python to develop predictive models 
 Strong skills in statistical methodologies such as A/B test, Experiment design, Hypothesis test, ANOVA, Cross Tabs, T tests and Correlation Techniques 
 Extensively worked on Python 3.5/2.7 (Numpy, Pandas, Matplotlib, Tensor flow, NLTK and Scikit-learn) 
 Worked on Tableau, Quick View to create dashboards and visualizations. 
 Experience in implementing data analysis with various analytic tools, such as Anaconda 4.0 Jupyter Notebook 4.X, R 3.0(ggplot2) and Excel 
 Good knowledge on cloud computing platform services platforms like AWS and Microsoft Azure 
 Strong expertise in using Tableau software as applied to BI data analytics, reporting and dashboard projects 
 Configured big data processing platform using Apache Spark, created predictive models using MLib and deployed models for large scale use.

Work Experience
Data Scientist

OnBlick Inc - Irving, TX
December 2015 to Present

Description: As a Data Scientist at OnBlick the role is to follow the data and analyse, visualize, and model job search related data. In addition, build and implement machine learning models to make timely decisions. 
Problem Statement: OnBlick is an online e-recruitment platform. Its objective is to increase the efficiency of the recruitment process by automating the process of analyzing the applicant profiles to determine the one's that fit the position specifications. Worked on applicant ranking and personality mining. 
Responsibilities: 
 Analysed business requirements and developed the applications, models, used appropriate algorithms for arriving at the required insights. 
 Used the Classification machine learning algorithms Naοve Bayes, Linear regression, Logistic regression, SVM, Neural Networks and used Clustering Algorithm K Means. 
 Conducted descriptive statistics, text analytics, exploratory analysis and data visualization with Python. 
 Using Natural Language Processing(NLP) and (ML)Machine Learning to rank the resumes based on the information extracted, according to the skill sets of the candidate and based on the job description of the company. 
 Worked on Text mining, text classification, sentiment analysis, Symantec analysis and Topic extraction to parse the resume one at a time 
 Worked on data cleaning, data preparation and feature engineering with Python 3.X 
 Worked on Map Reduce jobs in Hadoop and implemented Spark analysis using Python for performing machine learning & predictive analytics on AWS platform. 
 Implemented Lambda architecture (data-processing design pattern) to handle massive quantities of data and integrate batch and real-time processing within a single framework. This design pattern was implemented on AWS includes Amazon Kinesis, Amazon Simple Storage Service (Amazon S3), Spark Streaming, Pyspark and Spark SQL on top of an Amazon EMR cluster. 
 Extensively used Python's multiple data science packages like Pandas, NumPy, matplotlib, SciPy, Scikit-learn, Tensorflow and NLTK. 
 Worked on exploratory data analysis, data cleaning, visualization, Statistical Modeling using Python 3.5, R Studio, R Shiny and Tableau. 
 Created Hive scripts to create external, internal data tables on Hive. Worked on creating datasets to load data into HIVE. 
 Created the dashboards and reports in tableau for visualizing the data in required format. 
 The back propagation (BP) neural network is used as the algorithm of data mining and K means clustering is used to cluster the data and classification using back propagation algorithm have been analysed. 
 Worked on Apache spark for analyzing the live streaming data. 
 Understanding of working on Artificial Neural Networks and Deep Learning models using 
Theano and Tensor flow packages using in Python. 
Applications: Hadoop, Apache Spark, Hive, Machine learning, Jupyter notebook Python, Numpy, NLTK, Pandas, Scipy, SQL, Map Reduce, Tableau, Sqoop, HBase, Oozie, HDFS, PySpark, Tableau, DynamoDB, Mongo DB, SQL Server, and ETL.

Data Science Analyst

AMEX, Kansas City
June 2014 to May 2015

Description: Worked as a part of Analytical Group and worked on development and designing of statistical models for the company. Coordinated with team on designing and implementing data solutions as per project requirements. 
Problem Statement: To detect fraudulent transactions quickly and to find or predicting future customers in the customer relationship management market. 
Responsibilities: 
 Communicated and coordinated with other departments to collect business requirement 
 Handled importing data from various data sources, performed transformations using Hive, Map Reduce, and loaded data into HDFS. 
 Performed data analysis by using Hive to retrieve the data from Hadoop cluster, Sql to retrieve datafrom Oracle database. 
 Used pandas, numpy, seaborn, scipy, matplotlib, scikit-learn in Python for developing various machine learning algorithms. 
 Developed MapReduce pipeline for feature extraction using Hive. 
 Used Statistical testing to evaluate Model performance 
 Improved fraud prediction performance by using random forest and gradient boosting for feature selection with Python Scikit-learn 
 Implemented machine learning model (logistic regression, XGboost) with Python Scikit- learn 
 Designed rich data visualizations with Tableau and Python. 
Applications: Machine learning, Cassandra, NLTK, Spark, HDFS, Hive, Pig, Linux, Python (Scikit-Learn/Scipy/Numpy/Pandas), R, SAS, SPSS, MySQL, PL/SQL, Jupyter notebook, Tableau.

Data Science Analyst/Python Developer

United Health Group - Hyderabad, Telangana
June 2012 to December 2013

Responsibilities: 
 Analysed customer Help data, contact volumes, and other operational data in MySQL to provide insights that enable improvements to Help content and customer experience. 
 Brought in and implemented updated analytical methods such as regression modeling, classification tree, statistical tests and data visualization techniques with Python 
 Deployed Machine Learning Models built using mahout on Hadoop cluster 
 Maintained and updated existing automated solutions. 
 Analyzed historical demand, filter out outliers/exceptions, identify the most appropriate statistical forecasting algorithm, develop base plan, understand variance, propose improvement opportunities, and incorporate demand signal into forecast and executed data visualization by using plotly package in Python. 
 Improved data collection and distribution processes by using Pandas and Numpy packages in Python while enhancing reporting capabilities to provide clear line of sight into key performance trends and metrics. 
 Interacted with QA to develop test plans from high-level design documentation 
 Used Sqoop for loading existing data in Relational databases to HDFS. 
 
Applications: MySQL, Predictive modeling, Python libraries, pandas, Numpy packages, Hadoop, MapReduce, HDFS, Hive

Jr. Data Analyst

HSBC - Hyderabad, Telangana
July 2010 to May 2012

Responsibilities: 
 Gathered user requirements and created the business requirements documents. 
 Documented the technical specification for the reports and tested the generated reports. 
 Created and managed Databases. 
 Used the technical document to design tables. 
 Performed data analysis, data profiling, data scrubbing, data cleansing, generated data frequency reports. 
 Generated SQL and PL/SQL scripts to install, create, and drop database objects including tables, views, primary keys, indexes, constraints, packages, sequences, grants and synonyms. 
 Created Database triggers to maintain the audit data in the tables. 
 Optimized the SQL queries for improved performance. 
 Prepared test plans for various modules. 
 Prepared user manual and technical support manuals. 
 
Applications: Oracle 9i, SQL* Loader, PL/SQL, SQL

Skills
APACHE HADOOP HDFS (4 years), data analysis (5 years), Hadoop (4 years), MARKETING ANALYSIS (5 years), SQL (5 years)
Links
http://www.linkedin.com/in/priya-cds7

Additional Information
Technical Skills 
 
Programming Languages Python, SQL, C, PL/SQL, R (for data analysis) 
Predictive Modeling Technique 
Supervised Learning- Decision trees , Naive Bayes classification, Ordinary Least Squares regression, Logistic regression, Neural networks, Support vector machines Unsupervised Learning- Clustering Algorithms and Reinforcement Learning 
 
Statistical Methods t-test, Chi-squared and ANOVA testing, A/B Testing, Descriptive and Inferential Statistics, Hypothesis testing 
Analytics 
Python (Numpy, Pandas, Scipy, Scikit), statsmodels and Visualization: Matplotlib, seaborn, scikit-image), Big Data (HDFS, Pig, Hive, HBase, Sqoop, Spark), Excel, Tensorflow, Keras 
 
Reporting Tools Tableau, Spotfire, IBM Watson, QlickView 
Database Hadoop, Spark, Postgres, Access, Oracle, SQL Server, NoSQL (Mongo DB), Cassandra, HBase, Teradata, Dynamo DB. 
Cloud Computing Amazon AWS, Microsoft Azure, Google Analytics 
Version control: Git, GitHub