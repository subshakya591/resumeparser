Java Developer
Java Developer - BJC HealthCare
Irving, TX

Authorized to work in the US for any employer

Work Experience
Java Developer

BJC HealthCare - St. Louis, MO
June 2016 to Present

Responsibilities: 
 Hands on experience in J2EE framework and extensively used Core Java and Spring API in developing the business logic using Agile Methodology. 
 Implemented Model View Controller (MVC) Architecture based presentation using JSF framework. Worked on Servlets, JSP, Drools, JDBC and JavaScript under MVC Framework and implemented OOAD concept in the applications. 
 Extensive experience of developing Representational state transfer (REST) based services and Simple Object Access Protocol (SOAP) based services. Used Oracle as database and involved in the development of PL/SQL backend implementation and using SQL created Select, Update and Delete statements. 
 Developing or maintaining a production-level cloud-based architecture in AWS. 
 Worked on AWS, High Availability Practices and deploying backup/restore infrastructure 
 Extensively used the J2EE design patterns like Session Faηade, Business Object (BO), Service Locator, Data Transfer Object (DTO) and Data Access Object (DAO), Singleton. 
 Involved in writing EJBs (Stateless Session Beans) and Web Services for building the middleware distributed components and deployed them on application servers. 
 Developed RESTFUL web service and hands on experience in JSON parsing and XML parsing. Implemented the Hibernate framework (ORM) to interact with database created in MySQL. 
 Designed and developed web pages using HTML, JSP, JavaScript and XSLT, involved in writing new JSPs, designed pages using HTML and client validation using JavaScript and jQuery. 
 Performed Unit testing and Integration Testing using Junit. Involved in Agile methodology with respect to the successful development of the project. 
 Deployed GUI code to WebLogic application environment and standalone components to JBoss Server. Designing and developing Restful API and services using best practices to interact within the Microservices and with the front end using Spring MVC and Spring REST. 
 Implemented GUI pages using AngularJS, ReactJS, JavaScript, HTML and CSS. Developed Web Services to send data in XML, JSON format to the database on the supplier information. Supported the applications through debugging, fixing and maintenance releases. 
 Involved in mapping the data from various vendors with the existing database. Responsible for updating the supplier database if new updates are available. Responsible for requirements gathering, analyzing and developing design documents and reviewing with business. Involved in Units integration, bug fixing and User acceptance testing with test cases. 
 
Environment: Java, J2EE, Servlets, JSF's, jQuery, AWS, Spring (Spring MVC, Spring Annotations, Spring AOP), Microsoft SQL Server 2008, SOAP Web services, WebLogic Server 8.1/9.x/10.x, Log4J, JDBC, Spring JDBC, JUnit, Ext.JS, XML, Hibernate, Swing, WCS, Windows, JavaScript, AngularJS, ReactJS, Node.js, AJAX, REST, PL/SQL, CSS.

Java Developer

United Community Bank - Alpharetta, GA
December 2014 to May 2016

Responsibilities: 
 Used Microsoft Visio for designing the Use Case Diagrams, Class model, Sequence diagrams, and Activity diagrams for SDLC process of the application. Implemented GUI pages by using JSP, HTML, CSS, JavaScript, AngularJS and AJAX. 
 Implemented the project by using Spring Web MVC. Used handler mapping technique for mapping the various objects and communication with the database. Used JavaScript, AngularJS, and AJAX technologies for front end user input validations and Restful web service calls. 
 Tested and implemented the Web Services by using SOAP UI. Used it to send and receive requests in XML form. 
 Implemented the online application by using Core Java, JSP, Servlets, Web Services, SOAP, and WSDL. 
 Implemented Singleton, factory design pattern, DAO Design Patterns based on the application requirements. 
 Tested the Service classes and DAO Classes by using Unit Test Cases. Used REST Web Services to consume and produce JSON. Integrated REST Web Services with services and DAO layer. 
 Implemented complex queries with Hibernate for DAO layer while communicating with the database. 
 Monitored the error logs using Log4j and fixed the problems. 
 Experience using and creating custom scripts and applications using the AWS SDK and/or CLI Tools. 
 Experience in developing scripts using Restful API models for the purposes of integrating workflows with AWS. Fixed various existing UI's to meet the new design (VDL) standards. 
 Implemented JavaScript for creating new validation to existing functionality and improve the overall performance. Implemented validation helper classes and integrated with existing services. 
 Used Eclipse as Development IDE for web applications. Designed and developed base classes, framework classes and common re-usable components. 
 Configured the project on Web Sphere application servers. Used Git for Version Control tool to store the code in the repository and Clear Quest for bug tracking tool. 
 Deployed the project in Windows environment. Involved in multitier J2EE design using Spring IOC architecture and developed PL/SQL scripts to fetch and update Oracle database. 
Environment: Java, J2EE, JSP, Web Sphere, AWS, JDBC, Spring, XSLT, CSS, HTML, Web Services, WSDL, SOAP, RAD, SQL, PL/SQL, JavaScript, AngularJS, DHTML, XHTML, Git, Oracle11g, Toad, Log4j, Maven, Windows 7.

Java Developer

Selective Insurance Group, Inc - Branchville, NJ
December 2013 to November 2014

Responsibilities: 
 Involved in SDLC Requirements gathering, Analysis, Design, Development and Testing of application using AGILE methodology (SCRUM). 
 Involved in design discussions and understanding of business requirements and identify the relevant functional and technical requirements. 
 Used JSP's HTML on front end, Servlets as Front Controllers and JavaScript for client side validations. Created controller Servlets in JAVA for handling HTTP requests from JSP pages. 
 Implemented the business logic using various Design Patterns. 
 Used Eclipse IDE to develop/create my code. Developed entire application web-based client implementing MVC Architecture using Spring Frame work. 
 Design, investigation and implementation of public facing websites on Amazon Web Services (AWS). Developed Webpages using UI frameworks, AngularJS and Ext JS, Node JS, Backbone JS, Passport JS. Implemented Object-relation mapping in the persistence layer using Hibernate frame work in conjunction with Spring Functionality. 
 Developed several REST web services supporting both XML and JSON to perform tasks such as remote thermostat and demand-response management. Restful web services leveraged by both web and mobile applications. Integrated process into build system which auto-generates Restful API documentation from source code improving API documentation quality and availability while reducing maintenance costs. 
 Created Maven archetypes for generating fully functional Restful web services supporting both XML and JSON message transformation. Used SQL Queries in Java code to do select/update/delete depending upon the business requirement. Extensively JSP to develop schema viewer applications. 
 Used Git as source control management giving a huge speed advantage on centralized systems that should communicate with a server. 
Environment: Java, Java Beans, J2EE, SOAP, AWS, Spring, CXF, Web Logic, Hibernate, Ajax, AngularJS, jQuery, JSP, Web Sphere, DB2, JUnit, Log4J, Unix, Git, Soap-UI, FileZilla, Spring Framework security using LDAP, Maven.

Java Developer

Praxis Technologies
May 2011 to July 2013

Responsibilities: 
 Implemented the project as per the Software Development Life Cycle (SDLC). Used MVC Design Pattern architecture for designing the application. 
 Used Microsoft Visio for creating the initial UML Diagrams such as Use-Case Diagrams, Class Diagrams, Sequence Diagrams. 
 Implemented JDBC for mapping an object-oriented domain model to a traditional relational database created in MySQL. Involved in analyzing, designing, implementing and testing of the project. 
 Used Struts - Hibernate for loading data into the database. Used JUnit for unit testing the codes. 
 Developed UML diagrams like Use cases and Sequence diagrams as per requirement. 
 Used GIT for Version Control. Wrote SQL, PL/SQL to handle business functionality. 
 Created Functional Test cases and achieved bug fixes. 
 Project was developed following Agile and Scrum methodologies. Worked on Eclipse for development and deployment of application in Tomcat Application Server. 
 Designed and developed user interfaces using JSP, Java script, HTML, CSS. Involved in Database design and developing SQL Queries, stored procedures on MySQL. 
 Developed user and technical documentation. Code review and function testing for better client interface and usability. Participation in meeting with team, senior management and client stakeholders. 
 
Environment: JAVA, Struts, Hibernate, Java Script, HTML, Microsoft Visio, Eclipse, Junit, JDBC Drivers, MySQL, Git, SQL Server, Tomcat Server, CSS.

Education
Bachelors of Engineering in Information Technology

University of Mumbai Mumbai, Maharashtra
Skills
ARCHITECTURE (6 years), HTML (6 years), JAVA (6 years), MODEL VIEW CONTROLLER (6 years), MODEL-VIEW-CONTROLLER (6 years)
Additional Information
TECHNOLOGIES: 
SKILLS TOOLS 
Programming Language Java/J2EE, JavaScript, HTML, CSS, C++, Python, C, PL/SQL, SQL, DHTML, JSTL 
Databases Oracle 10g, 11g, SQL server [] MySQL, MongoDB, PL/SQL. 
Web Component HTML5, XML, AJAX, JavaScript, Node.js, AngularJS, JavaScript, jQuery, CSS3, Bootstrap, REST. 
Operating Systems Windows [] UNIX, Linux. 
Architecture & Framework 
Core JAVA, Spring core, Spring boot, Spring AOP, Spring Transaction, Spring MVC, Servlets, Struts, JSF, Hibernate ORM, JSF, JPA, JNDI, JTA, JAXP, Ajax, UML. 
 
IDE's Eclipse, My Eclipse, NetBeans, IntelliJ IDEA, Putty, File Zilla, Android Studio. 
Version Control SVN, GIT, CVS, ClearCase, VSS. 
Servers Apache Tomcat, JBoss, BEA WebLogic, Web sphere, Jenkins. 
Tools SQL Developer, Maven, Ant, Log4J, JUnit, SoapUI 4.0.5, Visio, Mockito, JDeveloper, JBuilder.