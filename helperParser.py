import re
class HelperParser:

    @staticmethod
    def regex_parser(text,mode):
        if mode=="personal_info":
            regex_email = r"([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)"
            regex_phone = r'\(?\+?\d{1,3}\)?-?\d{3}-?\d{3}-?\d{2,10}'
            return (re.findall(regex_email,text),re.findall(regex_phone,text))



