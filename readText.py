import re
import PyPDF2
import docx2txt
from stanfordNER import StanfordNER

class PdfToText:
    @staticmethod
    def startConversion(fileName):
        pdfFileObj = open(fileName,'rb')     #'rb' for read binary mode
        pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
        noOfPages=pdfReader.numPages
        text=""
        for pages in range(0,pdfReader.numPages):
            pageObj = pdfReader.getPage(pages)
            text+=pageObj.extractText()
            text = re.sub(r' +', ' ', text)
            text = re.sub(r'\t', '\n,', text)
            text = re.sub(r'\n+', '\n,', text)
            text = re.sub(r',+', ',', text)
            text=text.encode('ascii', errors='ignore').decode("utf-8")
        name, adress = StanfordNER.ner_parser(text, "personal_info")
        return text

class DocxToText:
    @staticmethod
    def startConversion(fileName):
        text=""
        text+=docx2txt.process(fileName)
        text = re.sub(r' +', ' ', text)
        text = re.sub(r'\t', '\n', text)
        text = re.sub(r'\n+', '\n', text)
        text=re.sub(r',+',',',text)
        text=text.encode('ascii', errors='ignore').decode("utf-8")
        return text

class TextExtractor:
    @staticmethod
    def startConversion(fileName):
        print(fileName)
        text=""
        with open(fileName, mode='r', encoding = 'unicode_escape', errors='strict', buffering=1) as file:
            data =file.read()
        text+=data
        text = re.sub(r' +', ' ', text)
        text = re.sub(r'\t', '\n', text)
        text = re.sub(r'\n+', '\n', text)
        text = re.sub(r',+', ',', text)
        text=text.encode('ascii', errors='ignore').decode("utf-8")
        # name,adress = StanfordNER.ner_parser(text, "personal_info")
        return text