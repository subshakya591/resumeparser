from segmentcreator import ResumeSegmentCreator
from helperParser import HelperParser
from stanfordNER import StanfordNER
from deeplearningfilter import NaiveFilter
from rulebasedsys import RulebasedParser
from NLTKHelper import NltkHelper
from subodhlibrary.shakyahelper import SSHELPER
print("""*******************Author: @Subodh_Chandra_Shakya*************""")

class InformationParser:

    def __init__(self,information,raw_text):
        self.resume_segment=information
        self.raw_text=raw_text

    def personal_information_parser(self):
        text=self.resume_segment["profile"]

        if text:
            emails,phone=HelperParser.regex_parser(text,"personal_info")
            name,address=StanfordNER.ner_parser(text,"personal_info")
            return (name,address,emails,phone)
        # else:
        #     return("Anonymous",["not found"],["not found"],["not found"])
        #     name=NltkHelper.name_chunker(self.raw_text)
        #     print(name)
        #     emails,phone=HelperParser.regex_parser(self.raw_text,"personal_info")
        #     return name,"gg",emails,phone

    def objective_parser(self):
        objectives=[]
        text=self.resume_segment["objectives"]
        if text:
            for sentence in text.split('\n'):
                label=NaiveFilter.naivefilter("objective",sentence)
                if label=="selfdescription":
                    objectives.append(sentence)
        print("<+++++++++@Objective_parsed_sucessfully+++++++++++>")
        return objectives

    def skills_parser(self):
        skills=[]
        technical_skills=[]
        non_technical_skills=[]
        residue_skills=[]
        text=self.resume_segment["skills"]
        if text:
            chunked_skills=NltkHelper.nltk_skill_chunker(text)
            for word in chunked_skills:
                label=NaiveFilter.naivefilter("skills",word)
                if label=="skillname":
                    skills.append(word)
                else:
                    residue_skills.append(word)

        # language = RulebasedParser.language_parser(skills)


        technical_skills,non_technical_skills=RulebasedParser.skill_separator(skills,residue_skills)
        return skills

    def experience_parser(self):
        experience= {}
        text = self.resume_segment["experiences"]
        print("textt",text)

        if text:
            print("inside experience")
            experience = (StanfordNER.ner_parser(text, "experience"))
            print("<+++++++++@Education_parsed_sucessfully++++++++++++>")
            return experience


    def education_parser(self):

        academics={}
        text=self.resume_segment["academics"]
        print("texter",text)
        if text:
            print("inside education")
            academics=(StanfordNER.ner_parser(text,"academics"))
            print("<+++++++++@Education_parsed_sucessfully++++++++++++>")

        return academics

    def language_parser(self):
        language=["english"]
        text=self.resume_segment["languages"]
        print("texttt",text)
        if text:
            language=RulebasedParser.language_parser(text)
            return language
        else:
            return language


    def project_parser(self):
        text=self.resume_segment["projects"]
        if text:
            projects=text
            return projects
        else:
            return (["projects not found"])


    def references_parser(self):
        text=self.resume_segment["references"]
        if text:
            references=text
            return text
        else:
            return (["references not available"])


    def rewards_parser(self):
        text=self.resume_segment["rewards"]
        if text:
            return text
        else:
            return(["rewards not found"])









