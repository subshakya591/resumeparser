import nltk
from string import punctuation

class SSHELPER:
    @staticmethod
    def hasNumbers(input):#function checks if sentence contains number or not
        return any(character.isdigit() for character in input)

    @staticmethod
    def hasPunctuation(input):#function to check whether punctuation is present or not
        return any(character in input for character in punctuation)

    @staticmethod
    def isnotBlank(string):
        print("here")
        print("bool", bool(string and string.strip()))

    @staticmethod
    def onlycontainsfullstop(inputString):
        except_punctuation = [",", "?", ";", ":", "!"]
        decision = 0
        ex_flag = 0
        for punct in except_punctuation:
            if punct not in nltk.word_tokenize(inputString):
                ex_flag += 1
        if ex_flag == len(except_punctuation):
            decision = 1
        if decision == 1:
            return True
        else:
            return False