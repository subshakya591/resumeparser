import nltk
from nltk import Tree,ne_chunk

class NltkHelper:

    @staticmethod
    def nltk_skill_chunker(section):
        chunked_phrases=[]
        chunk_noun_phrase="noun_phrase:{<NNP|NN|NNS><IN>?<NNP|NN>*[-|:]?<CD>?}"
        cp=nltk.RegexpParser(chunk_noun_phrase)
        for line in section.split('\n'):
            tokens=nltk.word_tokenize(line)
            tagged_tokens=nltk.pos_tag(tokens)
            if tagged_tokens:
                chunked_noun_phrase=cp.parse(tagged_tokens)
                for subtree in chunked_noun_phrase:
                    if type(subtree) == Tree and subtree.label() == "noun_phrase":
                        chunked_phrases.append(" ".join([token for token, pos in subtree.leaves()]))

        return(list(set(chunked_phrases)))

    # @staticmethod
    # def name_chunker(raw_text):
    #     possible_name=[]
    #     chunked_phrases=[]
    #     chunked_proper_noun="proper_noun:{<NNP|NN><NNP|NN>*}"
    #     cp=nltk.RegexpParser(chunked_proper_noun)
    #     indexed_text={}
    #     filtered_name=[]
    #     idx=0
    #     for line in raw_text.split('\n'):
    #         indexed_text.update({line:idx})
    #         idx+=1
    #         tokens=nltk.word_tokenize(line)
    #
    #         tagged_tokens=nltk.pos_tag(tokens)
    #         print("tokens", tagged_tokens)
    #         if tagged_tokens:
    #             chunked_proper_noun=cp.parse(tagged_tokens)
    #             for subtree in chunked_proper_noun:
    #                 if type(subtree)==Tree and subtree.label()=="proper_noun":
    #                     chunked_phrases.append(" ".join([token for token, pos in subtree.leaves()]))
    #         else:
    #             pass
    #         print("chunked",chunked_phrases)
    #
    #     for item in chunked_phrases:
    #         print(chunked_phrases,"chunked")
    #         tokenized_chunk=item.split(' ')
    #         if len(tokenized_chunk)>1 and len(tokenized_chunk)<4:
    #             possible_name.append(item)
    #
    #
    #         for name in possible_name:
    #
    #             try:
    #                 index=indexed_text[name]
    #                 print(name,index)
    #                 if index<25:
    #                     filtered_name.append(name)
    #                 else:
    #                     pass
    #             except Exception as e:
    #                 print("strig",str(e))
    #                 pass















