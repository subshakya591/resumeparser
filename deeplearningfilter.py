import os
import pickle
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
class NaiveFilter:

    @staticmethod
    #naive bayes filter for finding the true label of the sentence
    def naivefilter(classify,sentence):

        complex=['objective','projects','experience','rewards']
        simple=['skills','languages','educations']
        if classify.lower() in complex:
            model_name=open(os.getcwd()+'/models/complex.pickle','rb')
            file=os.getcwd()+'/classifierdataset/complex_dataset.csv'
        elif classify.lower() in simple:
            model_name = open(os.getcwd() + '/models/UniComDegree.pickle','rb')
            file = os.getcwd() + '/classifierdataset/datasets.csv'
        else:
            return 0
        model=pickle.load(model_name) #loads the pickle file for classifying the sentence
        df = pd.read_csv(file)
        X_train, X_test, y_train, y_test = train_test_split(df['data'], df['label'], random_state=0)
        count_vect = CountVectorizer()
        X_train_counts=count_vect.fit_transform(X_train)
        tfidf_transformer = TfidfTransformer()
        X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
        print("senttttt",sentence)
        label=model.predict(count_vect.transform([sentence])) #predicts the label of the
        # sentence whether it's university,objectives etc
        return label

# print(NaiveFilter.naivefilter('objective','To work in a challenging situation that will bring the best out of my ability & my innovative ideas will be best utilized.'))
#
# # class NLTKfilter:
#
#      @staticmethod
#      def nltkfilter():



