import nltk
from nltk.tag.stanford import StanfordNERTagger
import os

class StanfordNER:

    @staticmethod
    def ner_parser(text,mode):


        jar = './stanford-ner-tagger/stanford-ner.jar'
        model_academics = './stanford-ner-tagger/resume.ser.gz'
        model_personal='./stanford-ner-tagger/english.all.3class.distsim.crf.ser.gz'
        ner_tagger_personal= StanfordNERTagger(model_personal, jar, encoding='utf8')
        ner_tagger_academics=StanfordNERTagger(model_academics, jar, encoding='utf8')
        words = nltk.word_tokenize(text)


        if mode=="personal_info":
            tagged_tuples = (ner_tagger_personal.tag(words))
            print("taggersdfe", tagged_tuples)
            name,address=StanfordNER.personal_info_parser(tagged_tuples)
            return name,address
        elif mode=="academics":
            tagged_tuples = (ner_tagger_academics.tag(words))
            print("taggersdfe", tagged_tuples)
            print("txt",text)
            academics=StanfordNER.education_parser(tagged_tuples)
            return academics
        elif mode=="experience":
            tagged_tuples=(ner_tagger_academics.tag(words))
            experience=StanfordNER.experience_parser(tagged_tuples)
            return experience

    @staticmethod
    def personal_info_parser(tagged_tuples):
        name = []
        address = []
        possible_name = []
        possible_address = []
        print("iam in")
        for word, tag in tagged_tuples:
            if tag == "PERSON":
                possible_name.append(word)
            else:
                if possible_name:
                    name.append(" ".join(possible_name))
                    possible_name = []
        for word, tag in tagged_tuples:
            if tag == "LOCATION":
                possible_address.append(word)
            else:
                if possible_address:
                    address.append(" ".join(possible_address))
                    possible_address = []
        if possible_address:
            address.append(" ".join(possible_address))

        return name, address

    @staticmethod
    def education_parser(tagged_tuples):
        # print("taggedtup",tagged_tuples)
        possible_university=[]
        possible_degree=[]
        possible_date=[]
        university_name={}
        university_degree={}
        date={}
        deg_idx=0
        date_idx=0
        uni_idx=0
        academics={}

        for word,tag in tagged_tuples:
            if tag=="DEGREE":
                possible_degree.append(word)
            else:
                if possible_degree:
                    university_degree.update({" ".join(possible_degree):deg_idx})
                    deg_idx+=1
                    possible_degree=[]


        for word,tag in tagged_tuples:
            if tag=="DATE":
                possible_date.append(word)
            else:
                if possible_date:
                    date.update({" ".join(possible_date):date_idx})
                    date_idx+=1
                    possible_date = []
        if possible_date:
            date.update({" ".join(possible_date):date_idx})
            date_idx+=1

        for word,tag in tagged_tuples:
            if tag=="UNIVERSITY":
                possible_university.append(word)
            else:
                if possible_university:
                    university_name.update({" ".join(possible_university):uni_idx})
                    uni_idx+=1
                    possible_university=[]
        if possible_university:
            university_name.update({" ".join(possible_university):date_idx})

        x=1
        for key,value in university_degree.items():
            academics.update({"E{}".format(x):{"Degree":key}})
            for ky,val in university_name.items():
                if value==val:
                    academics["E{}".format(x)].update({"university":ky})
            for dky,dval in date.items():
                if len(university_degree)==len(date) and value==dval:
                    print("here")
                    academics["E{}".format(x)].update({"graduated_year":dky})
                    break
                else:
                    print("else h")
                    enrolled_date={}
                    exit_date={}
                    i=0
                    en_count=0
                    ex_count=0
                    for k,v in date.items():
                        if i%2==0:
                            enrolled_date.update({k:en_count})
                            i+=1
                            en_count+=1
                        else:
                            exit_date.update({k:ex_count})
                            ex_count+=1
                            i+=1
                            for ek,ev in enrolled_date.items():
                                if value==ev:
                                    academics["E{}".format(x)].update({"enrolled_date":ek})
                            for gk,gv in exit_date.items():
                                if value==gv:
                                    academics["E{}".format(x)].update({"graduated_date":gk})
                    print(enrolled_date)
                    print(exit_date)
            x+=1
        print("ac",academics)
        return(academics)

    @staticmethod
    def experience_parser(tagged_tuples):
        print("tagged_tuples",tagged_tuples)
        possible_company = []
        possible_designation = []
        possible_date = []
        ex_company_name = {}
        designation = {}
        date = {}
        deg_idx = 0
        date_idx = 0
        com_idx = 0
        experience = {}

        for word, tag in tagged_tuples:
            if tag == "DESIGNATION":
                possible_designation.append(word)
            else:
                if possible_designation:
                    designation.update({" ".join(possible_designation): deg_idx})
                    deg_idx += 1
                    possible_designation = []

        for word, tag in tagged_tuples:
            if tag == "DATE":
                possible_date.append(word)
            else:
                if possible_date:
                    date.update({" ".join(possible_date): date_idx})
                    date_idx += 1
                    possible_date = []
        if possible_date:
            date.update({" ".join(possible_date): date_idx})
            date_idx += 1

        for word, tag in tagged_tuples:
            if tag == "COMPANY":
                possible_company.append(word)
            else:
                if possible_company:
                    ex_company_name.update({" ".join(possible_company): com_idx})
                    com_idx += 1
                    possible_company = []
        if possible_company:
            ex_company_name.update({" ".join(possible_company): date_idx})

        x = 1
        for key, value in designation.items():
            experience.update({"Exp{}".format(x): {"Designation": key}})
            for ky, val in ex_company_name.items():
                if value == val:
                    experience["Exp{}".format(x)].update({"company": ky})
            for dky, dval in date.items():
                if len(designation) == len(date) and value == dval:
                    print("here")
                    experience["Exp{}".format(x)].update({"exit year": dky})
                    break
                else:
                    print("else h")
                    entry_date = {}
                    exit_date = {}
                    i = 0
                    en_count = 0
                    ex_count = 0
                    for k, v in date.items():
                        if i % 2 == 0:
                            entry_date.update({k: en_count})
                            i += 1
                            en_count += 1
                        else:
                            exit_date.update({k: ex_count})
                            ex_count += 1
                            i += 1
                            for ek, ev in entry_date.items():
                                if value == ev:
                                    experience["Exp{}".format(x)].update({"entry_date": ek})
                            for gk, gv in exit_date.items():
                                if value == gv:
                                    experience["Exp{}".format(x)].update({"exit_date": gk})
                    print(entry_date)
                    print(exit_date)
            x += 1
        print("ac", experience)
        return (experience)







