from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import  shutil
import pickle
import json
import nltk
from nltk.tag.stanford import StanfordNERTagger
import numpy as np
from fileReader import FileReader
from segmentcreator import ResumeSegmentCreator
from InformationParser import InformationParser




with open(os.getcwd()+'/models/segment_identifier.pkl','rb')as pkl:
    model=pickle.load(pkl)

with open(os.getcwd()+"/models/resume_identifier_new.pickle", "rb") as infile:
    identifier_model = pickle.load(infile)

jar = os.getcwd() + '/stanford-ner-tagger/stanford-ner.jar'
model_nltk = os.getcwd() + '/stanford-ner-tagger/resume.ser.gz'
ner_tagger = StanfordNERTagger(model_nltk, jar, encoding='utf8')



def resumeParser(file_path):
    cv_content = FileReader.read_cv(file_path)
    decision=resume_verifier(cv_content)
    if decision==1:
        output=parse(cv_content)
        status=True
    else:
        output="Not a resume"
        status=False
    return(output,status)

def resume_verifier(cv_content):
    resume_word = nltk.word_tokenize(cv_content)
    word_pos = ner_tagger.tag(resume_word)
    NER_list = [pos for word, pos in word_pos]
    present_NER = set(NER_list)
    resume_features = [int("TITLE" in present_NER),
                       int("PERSON" in present_NER),
                       int("LOCATION" in present_NER),
                       int("UNIVERSITY" in present_NER),
                       int("DEGREE" in present_NER),
                       int("DATE" in present_NER),
                       int("DESIGNATION" in present_NER),
                       int("COMPANY" in present_NER),
                       int("EXPERIENCE" in present_NER),
]

    # print(resume_features)
    resume_features = np.array([resume_features])

    status=identifier_model.predict(resume_features)[0]
    print("status",status)
    if status==1:
        return True
    else:
        return False


def parse(cv_content):
    resume_segment = ResumeSegmentCreator(model, cv_content)
    segment=resume_segment.format_segment()
    resume_parser = InformationParser(segment)  # referemces
    name,address,emails,phone=resume_parser.personal_information_parser()
    objective = resume_parser.objective_parser()
    skill = resume_parser.skills_parser()
    academics= resume_parser.education_parser()
    rewards = resume_parser.rewards_parser()
    # ex_company, designation, entry_date, exit_date = resume_parser.experience_parser()
    language = resume_parser.language_parser()
    references = resume_parser.references_parser()
    projects = resume_parser.project_parser()
    experience=resume_parser.experience_parser()
    if name:
        tokenized_name=name[0].split(' ')
        first_name=tokenized_name[0]
        last_name=tokenized_name[-1]


        if len(tokenized_name)==3:
            middle_name=tokenized_name[1]
        else:
            middle_name="Subodh"
    else:
        first_name="Subodh"
        middle_name="Chandra"
        last_name="Shakya"



    output=({
            'PERSONAL_INFORMATION':
                {
                    'First_Name': '{}'.format(first_name),
                    'Last_Name': '{}'.format(last_name),
                    'Middle_name':'{}'.format(middle_name),
                    'Email': emails,
                    'Phone_Number': phone,
                    'Address':address
                },
            'OBJECTIVE': objective,
            'SKILLS':
                {
                'Skills': skill
                },
            'EDUCATION':academics,
            'PROJECTS':projects,
            'REWARDS':rewards,
            'LANGUAGES': language,
            'REFERENCES': references


        }
    )
    with open('parsed_output.json','w')as resume_input:
         json.dump(output,resume_input,indent=4)
    return output






a=resumeParser(os.getcwd()+'/resume_input/amar.txt')

