import os,shutil
from readText import PdfToText,DocxToText,TextExtractor
import nltk
class FileReader:
    @staticmethod
    def read_cv(file_path):
        _,file_extension=(os.path.splitext(file_path))
        if file_extension=='.pdf':
            try:
                text=PdfToText.startConversion(file_path)
                return text
            except:
                pass
        elif file_extension==".docx":
            try:
                text=DocxToText.startConversion(file_path)
                return text
            except:
                pass
        elif file_extension==".txt":
            text=TextExtractor.startConversion(file_path)
            return text



