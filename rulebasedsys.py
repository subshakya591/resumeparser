import nltk
from subodhlibrary.shakyahelper import SSHELPER
class RulebasedParser:

    @staticmethod
    def language_parser(lan):
        language=['english']
        predefined_language = ["english", "hindi", "nepali", "russian",
                               "sanskrit", "chinese", "japanese", "italian",
                               "spanish", "mexican", "german","arabic",
                               "bengali","malay","portugese","french","punjabi",
                               "telgu","tamil"]
        for word in language:
            print(word)
        return list(set(language))

    @staticmethod
    def skill_separator(chunked_text,residue_skills):
        possible_technical_skill=[]
        possible_non_tech_skill=[]
        r_residue_skills=[]
        if residue_skills:
            r_residue_skills.extend(residue_skills)

        for skill_chunk in chunked_text:
            tokenized_skill=skill_chunk.split(' ')
            if (len(tokenized_skill)<=4 or SSHELPER.hasNumbers(skill_chunk))and not SSHELPER.hasPunctuation(skill_chunk):
                possible_technical_skill.append(skill_chunk)
            elif (len(tokenized_skill))>4:
                residue_skills.append(skill_chunk)
        print(possible_technical_skill)
        return possible_technical_skill,possible_non_tech_skill



